export const CHANGE_DESTINATION = "CHANGE_DESTINATION";

export function changeDestination(payload, gpsLoc, finalAmount) {
  return {
    type: CHANGE_DESTINATION,
    payload: payload,
    gpsLoc: gpsLoc,
    finalAmount: finalAmount
  };
}