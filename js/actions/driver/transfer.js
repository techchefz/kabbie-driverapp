export const TRANSFER_RIDE = "TRANSFER_RIDE";

export function transferRide(payload) {
  return {
    type: TRANSFER_RIDE,
    payload: payload,
  };
}