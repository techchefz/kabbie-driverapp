import { tripUpdate } from "../../services/driversocket";
import { endTripSubmit } from "../../middlewares/driver/endTripmiddleware";
import { changePageStatus } from "../../actions/driver/home";
import moment from "moment";
import config from "../../../config";


export const END_TRIP = "END_TRIP";
export const TRIP_FARE = "TRIP_FARE";

export function endTrip() {
  return (dispatch, getState) => {
    dispatch({ type: END_TRIP, payload: "endTrip" });
    fetchFareDetail(getState().driver, getState().basicAppConfig.config).then(
      fareAmt => {
        fetch(`${config.serverSideUrl}:${config.port}/api/trips/FinalTripAmount`,
          {
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
              Authorization: getState().driver.appState.jwtAccessToken,
            },
            body: JSON.stringify({ tripAmount: fareAmt, tripObj: getState().driver.trip, previousTripAmount: getState().driver.trip.tripAmt })
          }).then(resp => resp.json()).then((convertedData) => {
            dispatch({ type: TRIP_FARE, payload: convertedData.data.tripAmt }); //setting fare to reducer
            tripUpdate(getState().driver.trip); // socket call to notify end trip
            dispatch(changePageStatus("rateRider"));
          });
      }
    );
  };
}

export function fetchFareDetail(tripObj, appConfig) {

  return new Promise((resolve, reject) => {
    fetch(
      `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${
      tripObj.trip.srcLoc[0]
      },${tripObj.trip.srcLoc[1]}&destinations=${tripObj.user.gpsLoc[0]},${
      tripObj.user.gpsLoc[1]
      }&key=${appConfig.googleMapsApiKey}`,
      {
        method: "GET"
      }
    )
      .then(resp => resp.json())
      .then(response => {
        const bookingTime = moment(
          tripObj.trip.bookingTime,
          "YYYY-MM-DD'T'HH:mm:ss:SSSZ"
        );
        const currentTime = moment(new Date());
        const travelTime = moment.duration(currentTime.diff(bookingTime));
        const tripDistance = response.rows[0].elements[0].distance.value / 1000;
        let tripTime = Math.abs(travelTime._milliseconds);
        var tripAmt = 0;
        // tripAmt = tripDistance * appConfig.tripPrice.farePerKm +
        //     tripTime / 60000 * appConfig.tripPrice.farePerMin +
        //     appConfig.tripPrice.baseFare;
        if (tripObj.user.carDetails.type == "Quickest") {
          tripAmt = tripDistance * appConfig.quickestPrice.farePerKm +
            tripTime / 60000 * appConfig.quickestPrice.farePerMin +
            appConfig.quickestPrice.baseFare;
        } else if (tripObj.user.carDetails.type == "Sedan") {
          tripAmt = tripDistance * appConfig.sedanPrice.farePerKm +
            tripTime / 60000 * appConfig.sedanPrice.farePerMin +
            appConfig.sedanPrice.baseFare;
        } else if (tripObj.user.carDetails.type == "Luxury") {
          tripAmt = tripDistance * appConfig.luxuryPrice.farePerKm +
            tripTime / 60000 * appConfig.luxuryPrice.farePerMin +
            appConfig.luxuryPrice.baseFare;
        } else if (tripObj.user.carDetails.type == "Wagon") {
          tripAmt = tripDistance * appConfig.wagonPrice.farePerKm +
            tripTime / 60000 * appConfig.wagonPrice.farePerMin +
            appConfig.wagonPrice.baseFare;
        }else if (tripObj.user.carDetails.type == "Wheelchair") {
          tripAmt = tripDistance * appConfig.wheelchairPrice.farePerKm +
            tripTime / 60000 * appConfig.wheelchairPrice.farePerMin +
            appConfig.wheelchairPrice.baseFare;
        }else if (tripObj.user.carDetails.type == "Maxi") {
          tripAmt = tripDistance * appConfig.maxiPrice.farePerKm +
            tripTime / 60000 * appConfig.maxiPrice.farePerMin +
            appConfig.maxiPrice.baseFare;
        }
        resolve(Math.round(tripAmt));
        return tripAmt;
      })
      .catch(e => reject(e));
  });
}

export function fetchSubFareDetail(tripObj, appConfig) {

  return new Promise((resolve, reject) => {
    fetch(
      `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${tripObj.trip.srcLoc[0]},${tripObj.trip.srcLoc[1]}&destinations=${tripObj.user.gpsLoc[0]},${tripObj.user.gpsLoc[1]}&key=${appConfig.googleMapsApiKey}`,
      {
        method: "GET"
      }
    ).then(resp => resp.json())
      .then(response => {
        const bookingTime = moment(tripObj.trip.bookingTime, "YYYY-MM-DD'T'HH:mm:ss:SSSZ");
        const currentTime = moment(new Date());
        const travelTime = moment.duration(currentTime.diff(bookingTime));
        const tripDistance = response.rows[0].elements[0].distance.value / 1000;
        let tripTime = Math.abs(travelTime._milliseconds);
        var tripAmt = 0;
        // console.log('====================================');
        // console.log("change");
        // console.log('====================================');
        // trip = tripDistance * appConfig.tripPrice.farePerKm + tripTime / 60000 * appConfig.tripPrice.farePerMin

        if (tripObj.user.carDetails.type == "Economy") {
          tripAmt = tripDistance * appConfig.tripPrice.farePerKm + tripTime / 60000 * appConfig.tripPrice.farePerMin

        } else if (tripObj.user.carDetails.type == "Premium") {
          tripAmt = tripDistance * appConfig.tripPrice1.farePerKm + tripTime / 60000 * appConfig.tripPrice1.farePerMin

        } else if (tripObj.user.carDetails.type == "SUV") {
          tripAmt = tripDistance * appConfig.tripPrice2.farePerKm + tripTime / 60000 * appConfig.tripPrice2.farePerMin

        } else if (tripObj.user.carDetails.type == "Luxury") {
          tripAmt = tripDistance * appConfig.tripPrice3.farePerKm + tripTime / 60000 * appConfig.tripPrice3.farePerMin
        }
        resolve(Math.round(tripAmt));
        return tripAmt;
      })
      .catch(e => reject(e));
  });
}