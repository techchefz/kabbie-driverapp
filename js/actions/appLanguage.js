export const APP_LANGUAGE = "APP_LANGUAGE";

export function getAppLanguage(langType) {
    return {
      type: APP_LANGUAGE,
      payload: langType,
    };
  }