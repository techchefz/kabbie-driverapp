export const APP_INTRO_STATE = "APP_INTRO_STATE";

export function setAppIntroStatus(status) {
    return {
        type: APP_INTRO_STATE,
        payload: status
    }
}