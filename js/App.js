import React, { Component } from 'react';
import { StyleSheet, alert } from 'react-native';
import { Container, Content, Text, View, Root } from 'native-base';
import AppNavigator from './AppNavigator';
import Demo from "./components/driver/licenceDetails";
import Dam from "./components/common/otpVerification2";
class App extends Component {
  render() {
    return (
      <Root>
        <AppNavigator />
      </Root>
    );
  }
}

export default App;
