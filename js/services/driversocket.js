import { Toast } from "native-base";
import { storeObj } from "../setup";
import config from "../../config";
import {
  newRideRequest,
  socketDisconnected,
  syncDataAsync
} from "../actions/driver/home";
import { tripRequestUpdated } from "../actions/driver/pickRider";
import { tripStarted } from "../actions/driver/startRide";
import { newTripUpdate } from "../actions/driver/rateRider";
import { profileUpdated } from "../actions/driver/settings";
import { responseTimedOut } from "../actions/driver/rideRequest";
import { changeDestination } from "../actions/driver/changeDestination";
import { fetchSubFareDetail } from "../actions/driver/dropOff";
import { currentLocationDriver } from "../actions/driver/home";

import "../UserAgent";

const io = require("socket.io-client");

let socket = null;
export function socketDriverInit() {
  const { dispatch, getState } = storeObj.store;
  socket = io(`${config.serverSideUrl}:${config.port}`, {
    jsonp: false,
    transports: ["websocket"],
    query: `token=${storeObj.store.getState().driver.appState.jwtAccessToken}`
  });
  socket.heartbeatTimeout = 10000; // reconnect if not received heartbeat for 17 seconds
  socket.on("connect", () => {
    if (getState().driver.appState.socketDisconnected) {
      dispatch(syncDataAsync(getState().driver.appState.jwtAccessToken));
    }
    dispatch(socketDisconnected(false));
  });
  socket.on("disconnect", () => {
    dispatch(socketDisconnected(true));
    socket.connect();
  });
  socket.on("requestDriver", tripRequest => {
    dispatch(newRideRequest(tripRequest));
  });
  socket.on("tripRequestUpdated", tripRequest => {
    dispatch(tripRequestUpdated(tripRequest));
  });
  socket.on("tripUpdated", trip => {
    dispatch(newTripUpdate(trip));
  });
  socket.on("responseTimedOut", () => {
    dispatch(responseTimedOut());
  });
  //===========================changeDestination------------------------------------
  socket.on("changeDriverDestination", payload => {
    currentLocationDriver();
    // console.log('=================STATE LOC===================');
    // console.log(getState().driver.tripRequest);
    // console.log('====================================');
    fetchSubFareDetail(getState().driver, getState().basicAppConfig.config)
      .then((tripAmt) => {
        // console.log('======trip amnty==============================');
        // console.log(tripAmt);
        // console.log('====================================');
        fetch(`${config.serverSideUrl}:${config.port}/api/trips/subTripAmount`,
          {
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
              Authorization: getState().driver.appState.jwtAccessToken,
            },
            body: JSON.stringify({ tripAmount: tripAmt, tripObj: payload })
          }).then(resp => resp.json()).then((convertedData) => {
            // console.log('=================convertedData===================')
            // console.log(convertedData);
            // console.log('=================convertedData===================')
            dispatch(changeDestination(payload, getState().driver.trip.driver.gpsLoc, convertedData.data.tripAmt));
          });
      })
  });
  //===========================changeDestination------------------------------------

  socket.on("updateAvailable", user => {
    const updateUser = Object.assign(
      {},
      {
        data: user
      }
    );
    dispatch(profileUpdated(updateUser));
    Toast.show({
      text: "Status Updated",
      position: "bottom",
      duration: 1500
    });
  });
}

export function updateIsAvailable(user) {
  socket.emit("updateAvailable", user);
}

export function updateLocation(user, position) {
  // alert(user.gpsLoc[0])
  let positionMod = {
    coords: {
      latitude: 28.33,
      longitude: 77.22
    }
  }
  if (position) { user.position = position }
  else { user.position = positionMod }
  socket.emit("updateLocation", user);
}
export function requestDriverResponse(tripRequest) {
  socket.emit("requestDriverResponse", tripRequest);
}

export function tripRequestUpdate(tripRequest) {
  socket.emit("tripRequestUpdate", tripRequest);
}

export function startTrip(tripRequest) {
  socket.emit("startTrip", tripRequest, trip => {
    storeObj.store.dispatch(tripStarted(trip));
  });
}

export function tripUpdate(trip) {
  socket.emit("tripUpdate", trip);
}
