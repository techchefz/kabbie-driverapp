import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, Dimensions } from 'react-native';

class Slider extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{
                    width:'100%',
                }}>
                <ScrollView 
                    horizontal
                    pagingEnabled
                    bouncesZoom
                    showsHorizontalScrollIndicator={false}
                    style={{
                        flexDirection:'row'
                    }}
                    >
                    <Image
                        source={require('../../../../assets/images/Slider1.png')}
                        style={{
                            heigth: Dimensions.get('screen').width,
                            width: Dimensions.get('screen').width,
                            resizeMode: 'contain'
                        }} />
                    <Image
                        source={require('../../../../assets/images/Slider2.png')}
                        style={{
                            heigth: Dimensions.get('screen').width,
                            width: Dimensions.get('screen').width,
                            resizeMode: 'contain'
                        }} />
                    <Image
                        source={require('../../../../assets/images/Slider3.png')}
                        style={{
                            heigth: Dimensions.get('screen').width,
                            width: Dimensions.get('screen').width,
                            resizeMode: 'contain'
                        }} />
                </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFB600',
    },
});

export default Slider;
