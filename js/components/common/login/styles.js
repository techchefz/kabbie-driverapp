import commonColor from '../../../../native-base-theme/variables/commonColor';
const React = require('react-native');

const { Dimensions } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  iosLogoContainer: {
    alignItems: 'center',
    height:Dimensions.get('window').height * (1/2),
    justifyContent:'flex-end',
  },
  aLogoContainer: {
    alignItems: 'center',
    height:Dimensions.get('window').height * (1/2),
    justifyContent:'flex-end',
  },
  logoIcon: {
    width: "100%",
    height: 180,
    resizeMode:'contain',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  logoText: {
    color: '#fff',
    fontWeight: '700',
    fontSize: 25,
    lineHeight: 30,
    marginTop: -10
  },
  loginBtn: {
    // maxWidth:,
    height: 50,
    marginBottom: 15,
    paddingTop: 10,
    paddingBottom: 10,
    alignSelf: 'center',
    width: '80%',
    maxWidth: 400,
    backgroundColor: '#FFB600',
  },
  registerBtn: {
    height: 50,
    alignSelf: 'center',
    // minWidth: deviceWidth * (1/2),
    width: '80%',
    maxWidth: 400,
    backgroundColor: '#FFB600'
  }
};