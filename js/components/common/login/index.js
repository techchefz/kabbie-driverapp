import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Platform, StatusBar, Image, Dimensions, SafeAreaView } from 'react-native';
import {
  Content,
  Text,
  Button,
  Icon,
  Spinner,
  Thumbnail,
  Grid,
  Col
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import PropTypes from 'prop-types';

import Register from '../register/';
import SignIn from '../signIn/';
import { setActiveLogin } from '../../../actions/common/entrypage';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import ModalView from '../ModalView';
import IntroSlider from '../introSlider';

import styles from './styles';

const deviceHeight = Dimensions.get('window').height;

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      lang: this.props.language
    };
  }

  static propTypes = {
    setActiveLogin: PropTypes.func
  };
  state = {
    activeLogin: null
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.activeLogin !== null) {
      this.setState({
        activeLogin: nextProps.activeLogin
      });
    } else if (nextProps.activeLogin === null) {
      this.setState({
        activeLogin: null
      });
    }
  }
  componentWillMount() {
    setTimeout(() => {
      this.setState({ loading: false });
    }, 600);
  }
  render() {
    if (this.state.activeLogin === 'signin') {
      return <SignIn />;
    }
    if (this.state.activeLogin === 'register') {
      return <Register />;
    }

    if (this.state.loading) {
      return (
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        >
          <Spinner />
        </View>
      );
    } else {
      if (Object.keys(this.props.appConfig).length === 0) {
        return (
          <ModalView>
            <Text>No App Configuration Data</Text>
          </ModalView>
        );
      } else if (this.props.introSliderState == false) {
        return <IntroSlider />
      }
      else {
        return (
          <View style={{ flex: 1 }}>
          
            <StatusBar barStyle='dark-content' backgroundColor={'#FFB600'} />
            <Content scrollEnabled={false} style={{ backgroundColor: '#fff', height: Dimensions.get('window').height }}>
              <View
                style={Platform.OS === 'ios' ? (styles.iosLogoContainer) : (styles.aLogoContainer)}>
                <Thumbnail
                  source={require('../../../../assets/images/kabbie_logo.png')}
                  style={styles.logoIcon}
                />
                <Text style={styles.logoText}>Kabbie</Text>
              </View>
              <View style={{
                height: Dimensions.get('window').height * (1 / 2),
                justifyContent: 'flex-end',
                paddingBottom: Platform.OS === 'ios' ? 40 : 60
              }}>
                <Button
                  rounded
                  block
                  onPress={() => Actions.signIn()}
                  transparent
                  block
                  style={styles.loginBtn}>
                  <Text style={{ fontWeight: "600", color: "#000" }}>
                    SIGN IN
                  </Text>
                </Button>
                <Button
                  rounded
                  block
                  onPress={() => Actions.register()}
                  style={styles.registerBtn}>
                  <Text style={{ color: '#000' }}>
                    SIGN UP
                  </Text>
                </Button>
              </View>
            </Content>
          </View>
        );
      }
    }
  }
}


function mapStateToProps(state) {
  return {
    activeLogin: state.entrypage.active,
    appConfig: state.basicAppConfig.config,
    introSliderState: state.driver.appState.introSliderState
  };
}

function bindActions(dispatch) {
  return {
    setActiveLogin: page => dispatch(setActiveLogin(page))
  };
}

export default connect(mapStateToProps, bindActions)(Login);

