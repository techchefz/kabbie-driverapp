import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Text, Platform, Dimensions, Picker, Modal, TouchableOpacity } from "react-native";
import { Item, Input, Button, Grid, Col, View, Thumbnail } from "native-base";
import CountryPicker from "react-native-country-picker-modal";
import PropTypes from "prop-types";

import { registerAsync } from "../../../actions/common/register";
import Spinner from "../../loaders/Spinner";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import styles from "./styles";

const validate = values => {
  const errors = {};
  if (!values.fname || !/^[a-zA-Z]*$/.test(values.fname)) {
    errors.fname = `First name is Invalid`;
  }
  if (!values.lname || (!/^[a-zA-Z]*$/.test(values.lname))) {
    errors.lname = `Last name is Invalid`;
  }
  if (!values.email || (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email))) {
    errors.email = `Email is Invalid`;
  }
  if (!values.phoneNo || (!values.phoneNo.match(/^\d{10}$/))) {
    errors.phoneNo = `Phone number is Invalid`;
  }
  if (!values.password) {
    errors.password = `Password is Required`;

  }
  if (!values.confirmPassword) {
    errors.confirmPassword = `Confirm Password is Required`;

  }
  if ((values.password !== values.confirmPassword)) {
    errors.confirmPassword = `Confirm Password doesn't match`;
  }
  return errors;
};
export const input = props => {
  const { meta, input } = props;
  return (
    <View style={{ flex: 1, width: null }}>
      <Item
        style={{
          borderBottomWidth: input.name === "phoneNo" ? 0.5 : 0.5
        }}
      >
        <Input {...input} {...props} />
      </Item>

      {meta.touched &&
        meta.error && <Text style={{ color: "red" }}>{meta.error}</Text>}
    </View>
  );
};

class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cca2: "IN",
      callingCode: "91",
      name: "IN"
    };
  }
  static propTypes = {
    dispatch: PropTypes.func,
    error: PropTypes.string,
    handleSubmit: PropTypes.func,
    isFetching: PropTypes.bool
  };
  submit(values) {
    this.props.dispatch(
      registerAsync({ ...values, callingCode: this.state.callingCode })
    );
  }
  render() {
    return (
      <View>
        <View style={{ alignItems: 'center', marginBottom: 20 }}>
          <Thumbnail
            source={{ uri: 'http://enadcity.org/enadcity/wp-content/uploads/2017/02/profile-pictures.png' }}
            style={{ width: Dimensions.get('window').width * (1 / 3), height: Dimensions.get('window').width * (1 / 3), tintColor: '#000' }}
          />
        </View>
        <Grid>
          <Col style={{ padding: 10 }}>
            <Field
              component={input}
              name="fname"
              placeholderTextColor="#000"
              placeholder="First Name"
            />
          </Col>
          <Col style={{ padding: 10 }}>
            <Field
              component={input}
              name="lname"
              placeholder="Last Name"
              placeholderTextColor="#000"
            />
          </Col>
        </Grid>
        <View style={{ padding: 10 }}>
          <Field
            component={input}
            type="email"
            name="email"
            placeholder="Email"
            placeholderTextColor="#000"
            keyboardType="email-address"
            autoCapitalize="none"
          />
        </View>
        <View style={{ padding: 10, flexDirection: "row" }}>
          <View style={{
            width: "10%", justifyContent: "center",
            alignItems: "center", borderBottomWidth: 0.5, borderBottomColor: commonColor.brandPrimary
          }}>
            <CountryPicker
              cca2={this.state.cca2}
              onChange={value =>
                this.setState({
                  cca2: value.cca2,
                  callingCode: value.callingCode
                })
              }
            />
          </View>
          <View style={{ width: "90%" }}>
            <Field
              component={input}
              type="phoneNo"
              name="phoneNo"
              maxLength={10}
              minLength={10}
              placeholder="Mobile Number"
              placeholderTextColor="#000"
              keyboardType="numeric"
            />
          </View>
        </View>
        {/* <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View style={{ flex: 1, justifyContent: 'space-around', alignItems: 'center' }}>
            <View
              style={{
                backgroundColor: 'white',
                width: "80%",
                justifyContent: 'center',
                alignItems: 'center',
                height: 300,
                borderRadius: 10,
                ...Platform.select({
                  android: { elevation: 2 },
                  ios: {
                    shadowColor: 'black',
                    shadowOffset: { height: 0, width: 0 },
                    shadowOpacity: 0.8,
                    shadowRadius: 100,
                  }
                }),
              }}>
              <Text>Choose Your Car Type</Text>
              <View style={{ height: 200, margin: 10 }}>
                <Picker
                  selectedValue={this.state.carType}
                  style={{ height: 50, width: 100 }}
                  onValueChange={(itemValue, itemIndex) => this.setState({ carType: itemValue, carTypeError: false })}>
                  <Picker.Item label="Economy" value="Economy" />
                  <Picker.Item label="Luxury" value="Luxury" />
                  <Picker.Item label="SUV" value="SUV" />
                  <Picker.Item label="Premium" value="Premium" />
                </Picker>
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
                <Text>SELECT</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal> */}
        <View style={{ padding: 10 }}>
          <Field
            component={input}
            name="password"
            placeholder="Password"
            secureTextEntry

            placeholderTextColor="#000"
            autoCapitalize="none"
          />
        </View>
        <View style={{ padding: 10 }}>
          <Field
            component={input}
            name="confirmPassword"
            placeholder="Confirm Password"
            secureTextEntry

            placeholderTextColor="#000"
            autoCapitalize="none"
          />
        </View>
        {this.props.error &&
          <Text style={{ color: "red" }}>
            {this.props.error}
          </Text>}
        <View style={styles.regBtnContain}>
          <Button
            onPress={
              () => {
                this.state.carType == "Choose your car type" ?
                  this.setState({
                    carTypeError: true
                  }) : [this.setState({
                    carTypeError: false
                  }), this.props.handleSubmit(this.submit.bind(this))]
              }
            }
            onPress={this.props.handleSubmit(this.submit.bind(this))}
            block
            style={styles.regBtn}
          >
            {this.props.isFetching
              ? <Spinner />
              : <Text style={{ color: "#000", fontWeight: "bold" }}>
                Register
            </Text>}
          </Button>
        </View>
      </View>
    );
  }
}

export default reduxForm({ form: "register", validate })(RegisterForm);
