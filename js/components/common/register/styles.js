import commonColor from "../../../../native-base-theme/variables/commonColor";

export default {
  iosHeader: {
    backgroundColor: '#FFB600',
  },
  aHeader: {
    backgroundColor: '#FFB600',
    borderColor: '#aaa',
    elevation: 3,
  },
  iosHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    color: "#000",
  },
  aHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 26,
    marginTop: -5,
    color: "#000",
  },
  orText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
  },
  regBtnContain: {
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  regBtn: {
    borderRadius: 25,
    height: 50,
    backgroundColor: '#FFB600',
  },
  googleLeft: {
    flex: 1,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#B6382C",
    borderBottomLeftRadius: 4,
    borderTopLeftRadius: 4
  },
  fbLeft: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    backgroundColor: "#233772",
    borderBottomLeftRadius: 4,
    borderTopLeftRadius: 4
  }
};
