import * as Exponent from "expo";
import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Platform, SafeAreaView } from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import {
  Container,
  Content,
  Header,
  Text,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Item,
  Body,
  Toast,
  Grid, Col
} from "native-base";
import { Actions } from "react-native-router-flux";
import { requestFbLogin } from "../loginFb";
import { registerWithGoogleAsync } from "../loginGoogle";
import { checkUser, userLoginRequest } from "../../../actions/common/checkUser";
import * as appStateSelector from "../../../reducers/driver/appState";
import RegisterForm from "./form";
import RegisterFormFb from "./formFb";

import { registerAsync } from "../../../actions/common/register";
import {
  clearEntryPage,
  socailSignupSuccess
} from "../../../actions/common/entrypage";

import commonColor from "../../../../native-base-theme/variables/commonColor";
import styles from "./styles";

function mapStateToProps(state) {
  const getErrormsg = () => {
    if (!state.driver.appState.errormsg) {
      return "";
    } else return state.driver.appState.errormsg;
  };

  return {
    isLoggedIn: state.driver.appState.isLoggedIn,
    registerError: state.driver.appState.registerError,
    socialLogin: state.entrypage.socialLogin,
    appConfig: state.basicAppConfig.config,
    isFetching: appStateSelector.isFetching(state),
    errormsg: getErrormsg()
  };
}
class Register extends Component {
  static propTypes = {
    errormsg: PropTypes.string,
    isFetching: PropTypes.bool,
    socailSignupSuccess: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      checkDriver: false,
      registerError: false,
      FirstNameChange: "",
      LastNameChange: "",
      EmailChange: "",
      MobileNumberChange: "",
      PasswordChange: "",
      socialLogin: null
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.registerError) {
      this.setState({ registerError: true });
    }
    if (nextProps.socialLogin.email !== null) {
      this.setState({ socialLogin: nextProps.socialLogin });
    }
  }

  render() {
    return (
      <Container style={{ backgroundColor: "#fff" }}>
        <SafeAreaView style={{ backgroundColor: "#FFB600" }} />
        <Header
          height={45}
          iosBarStyle="dark-content"
          androidStatusBarColor="#FFB600"
          style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}>
          <Left>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon
                name="md-arrow-back"
                style={{
                  fontSize: 28,
                  marginLeft: 5,
                  color: "#000"
                }}
              />
            </Button>
          </Left>
          <Body>
            <Title
              style={
                Platform.OS === "ios" ? (
                  styles.iosHeaderTitle
                ) : (
                    styles.aHeaderTitle
                  )
              }>SIGN UP
            </Title>
          </Body>
          <Right />
        </Header>
        <Content
          style={{ padding: 10 }} scrollEnabled bounces={false}>
          <View style={{ padding: 10 }}>
            {this.state.socialLogin && (
              <RegisterFormFb socialLogin={this.state.socialLogin} />
            )}
            {!this.state.socialLogin && (
              <RegisterForm isFetching={this.props.isFetching} />
            )}
          </View>
        </Content>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    checkUser: (obj1, obj2) => dispatch(checkUser(obj1, obj2)),
    userLoginRequest: () => dispatch(userLoginRequest()),
    clearEntryPage: () => dispatch(clearEntryPage()),
    socailSignupSuccess: route => dispatch(socailSignupSuccess(route)),
    registerAsync: userCredentials => dispatch(registerAsync(userCredentials)),
    registerAsyncFb: userObj => dispatch(registerAsyncFb(userObj))
  };
}

export default connect(mapStateToProps, bindActions)(Register);
