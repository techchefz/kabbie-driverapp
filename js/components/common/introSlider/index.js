import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet, View, AsyncStorage, TouchableOpacity, StatusBar, Image } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import { Actions } from 'react-native-router-flux';
import { connect } from "react-redux";
import { setAppIntroStatus } from '../../../actions/appIntroState';

const slides = [
  {
    key: 'somethun',
    title: '',
    titleStyle: {
      color:'black'
    },
    text: 'Description.\nSay something cool',
    textStyle: {
      color:'black'
    },
    image: require('../../../../assets/images/Slider1.png'),
    imageStyle: {
      width: 300,
      height: 300,
      resizeMode: 'contain'
    },
    backgroundColor: 'transparent'
  },
  {
    key: 'somethun-dos',
    title: '',
    titleStyle: {
      color:'black'
    },
    text: 'Description.\nSay something cool',
    textStyle: {
      color:'black'
    },
    image: require('../../../../assets/images/Slider2.png'),
    imageStyle: {
      width: 300,
      height: 300,
      resizeMode: 'contain'
    },
    backgroundColor: 'transparent',
  },
  {
    key: 'somethun1',
    title: '',
    titleStyle: {
      color:'black'
    },
    text: 'Description.\nSay something cool',
    textStyle: {
      color:'black'
    },
    image: require('../../../../assets/images/Slider3.png'),
    imageStyle: {
      width: 300,
      height: 300,
      resizeMode: 'contain'
    },
    backgroundColor: 'transparent',
  }
];

class App extends React.Component {
  componentWillMount() {
    AsyncStorage.getItem("IntroStatus").then(respo => {
      // console.log("inside the 2 cwm agetItem")
      if (respo == "true") {
        this.props.dispatch(setAppIntroStatus(true)),
          Actions.login()
      }
    })
  }

  onSlideFinish() {
    AsyncStorage.setItem('IntroStatus', "true").then(
      this.props.dispatch(setAppIntroStatus(true)),
      Actions.login()
    )
  }

  _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Ionicons
          name="md-arrow-round-forward"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: 'transparent' }}
        />
      </View>
    );
  }
  _renderDoneButton = () => {
    return (
      <TouchableOpacity
        onPress={() => this.onSlideFinish()}
        style={styles.buttonCircle}>
        <Ionicons
          name="md-checkmark"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: 'transparent' }}
        />
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <View style={{flex:1, backgroundColor:"white"}}>
      <StatusBar barStyle="dark-content" backgroundColor={"#fff"}/>
      <Image 
        source={require('../../../../assets/images/kabbie_logo.png')}
        style={{height:100, width:"100%",resizeMode:"contain", position:'absolute', top:100}}/>
      <AppIntroSlider
        slides={slides}
        dotColor="#adadad"
        activeDotColor="#e4e4e4"
        renderDoneButton={this._renderDoneButton}
        renderNextButton={this._renderNextButton}
      />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function mapStateToProps(state) {
  return {
    introProps: state
  }
}

export default connect(mapStateToProps, null)(App)

