import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Platform, Dimensions, AsyncStorage } from 'react-native';
import { Header, Left, Right, Body } from "native-base";
import { Actions } from "react-native-router-flux";
import PropTypes from 'prop-types';
import { getAppLanguage } from '../../../actions/appLanguage'
import { connect } from 'react-redux';
import IntroSlider from '../introSlider';
import { setAppIntroStatus } from '../../../actions/appIntroState';


class ConfirmLanguage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedLanguage: 'en',
            confirmBtn: true,
            status: false,
            EOpacity: 1,
            IOpacity: 1
        }
    }
    componentWillMount() {
        AsyncStorage.getItem('Language').then(res => {

            if (res === 'en' || res === 'it') {
                this.props.dispatch(getAppLanguage(res)),
                    AsyncStorage.getItem("IntroStatus").then(respo => {
                        if (respo == "true") {
                            this.props.dispatch(setAppIntroStatus(true)),
                                Actions.login()
                        } else {
                            this.setState({ status: true })
                        }
                    })
            }
        })
    }
    onLanguageSelection() {
        AsyncStorage.setItem('Language', this.state.selectedLanguage).then(
            this.props.dispatch(getAppLanguage(this.state.selectedLanguage)),
        );
        this.setState({ status: true })
    }
    changeLangType(langType) {
        this.setState({ selectedLanguage: langType }),
            this.setState({
                confirmBtn: false
            })
    }
    render() {
        if (this.state.status) {
            return (
                <IntroSlider />
            );
        }
        else {
            return (
                <View style={styles.container}>
                    {/* -----------------------Header----------------------- */}
                    <Header
                        iosBarStyle="light-content"
                        androidStatusBarColor="#FFB600"
                        style={{
                            width: '100%',
                            backgroundColor: '#FFB600',
                            justifyContent: 'flex-end',
                            alignItems: 'center'
                        }}>

                        <Body>
                            <Text style={{
                                fontSize: 18,
                                color: '#fff',
                                backgroundColor: 'transparent'
                            }}>Choose Language / Scegli la lingua</Text>
                        </Body>

                    </Header>

                    {/* -----------------------Body----------------------- */}
                    <View style={{ flex: 8, backgroundColor: 'white', width: '100%', alignItems: 'center' }}>
                        <Image
                            source={require("../../../../assets/images/langIcon.png")}
                            style={{
                                height: Dimensions.get('window').width * (1 / 2),
                                width: Dimensions.get('window').width * (1 / 2),
                                tintColor: '#000'
                            }} />
                        <View style={{
                            flexDirection: 'row',
                            width: '90%',
                            justifyContent: 'space-around',
                            marginTop: 50
                        }}>
                            <TouchableOpacity onPress={() => [this.changeLangType('en'), this.setState({
                                IOpacity: 0.2,
                                EOpacity: 1
                            })]}>
                                <Image
                                    source={require('../../../../assets/images/England.png')}
                                    style={{
                                        height: 100,
                                        width: 100,
                                        resizeMode: 'contain',
                                        opacity: this.state.EOpacity
                                    }} />
                                <Text style={styles.LangText}>English</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => [this.changeLangType('it'), this.setState({
                                IOpacity: 1,
                                EOpacity: 0.2
                            })]}>
                                <Image
                                    source={require('../../../../assets/images/Italy.png')}
                                    style={{ height: 100, width: 100, resizeMode: 'contain', opacity: this.state.IOpacity }} />
                                <Text style={styles.LangText}>Italian</Text>
                            </TouchableOpacity>
                        </View>

                    </View>

                    {/* -----------------------Footer----------------------- */}
                    <View style={{ flex: 1, width: '100%', maxHeight: 60 }}>
                        <TouchableOpacity
                            style={styles.confirmbtn}
                            disabled={this.state.confirmBtn}
                            onPress={() => this.onLanguageSelection()}>
                            <Text style={{ backgroundColor: 'transparent', fontSize: 18, color: 'white' }}>CONFIRM</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }

    }
}

// define your styles

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    confirmbtn: {
        width: '100%',
        backgroundColor: '#FFB600',
        ...Platform.select({
            android: { elevation: 2 },
            ios: {
                shadowColor: 'black',
                shadowOffset: { height: 2, width: 2 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
            }
        }),
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
    },
    LangText: {
        textAlign: 'center',
        fontSize: 18,
        marginTop: 10
    }
});


function mapStateToProps(state) {
    return {
        language: state.driver.appState.language
    }
}
//make this component available to the app
export default connect(mapStateToProps)(ConfirmLanguage);
