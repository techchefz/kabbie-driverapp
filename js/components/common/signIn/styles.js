import commonColor from '../../../../native-base-theme/variables/commonColor';
import { Dimensions } from "react-native";
// const React = require('react-native');

// const { Dimensions } = React;
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  iosHeader: {
    backgroundColor: '#FFB600',

  },
  aHeader: {
    backgroundColor: '#FFB600',
    borderColor: '#aaa',
    elevation: 3,
  },
  iosHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    color: "#000",
  },
  aHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 26,
    marginTop: -5,
    color: "#000",
  },
  orText: {
    textAlign: 'center',
    fontWeight: '700',
    color: '#fff',
  },
  regBtnContain: {
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  regBtn: {
    height: 50,
    borderRadius: 25,
    backgroundColor: '#FFB600'

  },
  googleLeft: {
    flex: 1,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#B6382C",
    borderBottomLeftRadius: 4,
    borderTopLeftRadius: 4
  },
  fbLeft: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    backgroundColor: "#233772",
    borderBottomLeftRadius: 4,
    borderTopLeftRadius: 4
  }, logoIcon: {
    width: Dimensions.get('window').width * (1 / 2.5),
    height: Dimensions.get('window').width * (1 / 2.5),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20
  },
  logoText: {
    color: '#000',
    fontWeight: '700',
    fontSize: 25,
    lineHeight: 30,
    marginTop: -10
  },
  iosLogoContainer: {
    height: Dimensions.get('window').height * (1 / 3),
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 30,
  },
  aLogoContainer: {
    height: Dimensions.get('window').height * (1 / 3),
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 30,
  }
};