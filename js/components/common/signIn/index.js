import * as Exponent from "expo";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Platform, View, KeyboardAvoidingView, Dimensions, SafeAreaView, PixelRatio } from "react-native";
import FAIcon from "react-native-vector-icons/FontAwesome";
import PropTypes from "prop-types";
import {
  Container,
  Content,
  Header,
  Footer,
  Text,
  Button,
  Icon,
  Title,
  Left,
  Right,
  Item,
  Body,
  Toast,
  Grid, Col,
  Thumbnail
} from "native-base";
import { Actions } from "react-native-router-flux";
import RegisterFormFb from "../register/formFb";
import * as appStateSelector from "../../../reducers/driver/appState";
import LoginForm from "./form";
import { signinAsync } from "../../../actions/common/signin";
import {
  clearEntryPage,
  socailLoginSuccessAndRoutetoRegister,
  socailSignupSuccess
} from "../../../actions/common/entrypage";
import { requestFbLogin } from "../loginFb";
import { signInWithGoogleAsync } from "../loginGoogle";
import { checkUser, userLoginRequest } from "../../../actions/common/checkUser";

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";

function mapStateToProps(state) {
  const getErrormsg = () => {
    if (!state.driver.appState.loginError) {
      return "";
    } else return state.driver.appState.errormsg;
  };
  return {
    isLoggedIn: state.driver.appState.isLoggedIn,
    loginError: state.driver.appState.loginError,
    errormsg: appStateSelector.getErrormsg(state),
    isFetching: appStateSelector.isFetching(state),
    socialLogin: state.entrypage.socialLogin,
    appConfig: state.basicAppConfig.config
  };
}
class SignIn extends Component {
  static propTypes = {
    loginError: PropTypes.bool,
    errormsg: PropTypes.string,
    isFetching: PropTypes.bool,
    signinAsync: PropTypes.func,
    socailLoginSuccessAndRoutetoRegister: PropTypes.func,
    socailSignupSuccess: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      socialLogin: null
    };
  }
  state = {
    showError: false
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.loginError) {
      this.setState({
        showError: true
      });
    } else {
      this.setState({
        showError: false
      });
    }
    if (nextProps.socialLogin.email !== null) {
      this.setState({ socialLogin: nextProps.socialLogin });
    }
  }

  render() {
    return (
      <Container style={{ backgroundColor: "#fff" }}>
        <SafeAreaView style={{ backgroundColor: "#FFB600" }} />
        <Header
          height={45}
          iosBarStyle="dark-content"
          androidStatusBarColor="#FFB600"
          style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
        >
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon
                name="md-arrow-back"
                style={{
                  fontSize: 28,
                  marginLeft: 5,
                  color: "#000"
                }}
              />
            </Button>
          </Left>
          <Body style={{ flex: 8 }}>
            <Title
              style={
                Platform.OS === "ios"
                  ? styles.iosHeaderTitle
                  : styles.aHeaderTitle
              }
            >SIGN IN
            </Title>
          </Body>
          <Right style={{ flex: 1 }} />
        </Header>
        <Content showsVerticalScrollIndicator={false}>
          <KeyboardAvoidingView style={{ padding: 10 }} scrollEnabled bounces={false}>

            {/* -------------------------------------ICON-------------------------------------- */}
            <View style={{ height: Dimensions.get("window").height * (2 / 10), justifyContent: 'center', alignItems: 'center' }}>
              <Thumbnail
                source={require('../../../../assets/images/kabbie_logo.png')}
                style={{ width: "100%", height: "90%", resizeMode: 'contain' }} />
            </View>

            {/* -------------------------------------LOGIN TEXTINPUT-------------------------------------- */}
            <View style={{ width: '100%', height: Dimensions.get("window").height * (5 / 10), justifyContent: 'flex-end', }}>
              <View style={{ padding: 10 }}>
                {!this.state.socialLogin && (
                  <LoginForm isFetching={this.props.isFetching} />
                )}
              </View>
            </View>
          </KeyboardAvoidingView>
        </Content>
        <Footer style={{ backgroundColor: 'transparent' }}>
          <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
            <Text style={{ color: '#FFB600' }}>
              Dont have an account?
            </Text>
            <Text onPress={() => Actions.register()} style={{ color: '#E1BA52', fontWeight: 'bold' }}>
              &nbsp; SIGN UP
            </Text>
          </View>
        </Footer>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    checkUser: (obj1, obj2) => dispatch(checkUser(obj1, obj2)),
    userLoginRequest: () => dispatch(userLoginRequest()),
    clearEntryPage: () => dispatch(clearEntryPage()),
    socailLoginSuccessAndRoutetoRegister: data =>
      dispatch(socailLoginSuccessAndRoutetoRegister(data)),
    socailSignupSuccess: route => dispatch(socailSignupSuccess(route)),
    signinAsync: userCredentials => dispatch(signinAsync(userCredentials)),
    changePageStatus: pageStatus => dispatch(changePageStatus(pageStatus))
  };
}

export default connect(mapStateToProps, bindActions)(SignIn);
