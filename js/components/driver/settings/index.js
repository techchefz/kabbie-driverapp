import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Platform, SafeAreaView, TouchableOpacity } from "react-native";
// import FAIcon from "react-native-vector-icons/FontAwesome";
import { ImagePicker } from "expo";
import {
  Container,
  Header,
  Content,
  Button,
  Icon,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Item,
  Title,
  Left,
  Right,
  Spinner,
  Body
} from "native-base";
import { Actions } from "react-native-router-flux";

import SettingsForm from "./form";
import {
  updateUserProfileAsync,
  updateUserProfilePicAsync
} from "../../../actions/driver/settings";
import FAIcon from "react-native-vector-icons/MaterialCommunityIcons";
import styles from "./styles";

function mapStateToProps(state) {
  return {
    jwtAccessToken: state.driver.appState.jwtAccessToken,
    fname: state.driver.user.fname,
    lname: state.driver.user.lname,
    email: state.driver.user.email,
    profileUrl: state.driver.user.profileUrl,
    userDetails: state.driver.user,
    profileUpdating: state.driver.user.profileUpdating
  };
}
class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      submit: false,
      image: null
    };
  }

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      quality: 0.3
    });
    if (!result.cancelled) {
      this.setState({ image: result.uri });
      let userData = Object.assign(this.props.userDetails, {
        localUrl: result.uri
      });
      this.props.updateUserProfilePicAsync(userData, "profile");
    } else {
      this.setState({ image: this.props.profileUrl });
    }
  };
  render() {

    return (
      <Container style={{ backgroundColor: "#fff" }}>
        <SafeAreaView style={{ backgroundColor: "#FFB600", width: "100%" }} />
        <Header
          height={45}
          iosBarStyle="dark-content"
          androidStatusBarColor='#FFB600'
          style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
        >
          <Left>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon
                name="md-arrow-back"
                style={{ fontSize: 28, color: "#000" }}
              />
            </Button>
          </Left>
          <Body>
            <Title
              style={
                Platform.OS === "ios"
                  ? styles.iosHeaderTitle
                  : styles.aHeaderTitle
              }
            >
              Settings
            </Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Card
            style={{
              marginTop: 0,
              marginRight: 0,
              paddingTop: 20,
              paddingBottom: 20,
              marginLeft: 0
            }}
          >
            <CardItem style={{ padding: 0 }}>
              <Item
                style={{ paddingRight: 25, borderBottomWidth: 0 }}
              >
                <TouchableOpacity
                  onPress={this._pickImage}
                  style={{
                    position: "absolute",
                    right: 0,
                    top: 0
                  }}>
                  <FAIcon name="pencil-circle-outline" size={30} />
                </TouchableOpacity>
                {this.props.profileUpdating ? (
                  <View
                    style={{
                      width: 70,
                      height: 70,
                      borderRadius: 35,
                      alignItems: "center",
                      justifyContent: "center",
                      backgroundColor: "#eee"
                    }}
                  >
                    <Spinner />
                  </View>
                ) : (
                    <Thumbnail
                      source={{ uri: this.props.profileUrl }}
                      style={{ width: 70, height: 70, borderRadius: 35 }}
                    />
                  )}
              </Item>
              <View style={{ marginLeft: 20 }}>
                <Text
                  style={{
                    color: "#fff",
                    fontSize: 20,
                    fontWeight: "400",
                    color: "#0F517F"
                  }}
                >
                  {this.props.fname} {this.props.lname}
                </Text>
                <Text note style={{ color: "#6895B0" }}>
                  {this.props.email}
                </Text>
              </View>
            </CardItem>
          </Card>
          <SettingsForm />
        </Content>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    updateUserProfileAsync: userDetails =>
      dispatch(updateUserProfileAsync(userDetails)),
    updateUserProfilePicAsync: (userData, type) =>
      dispatch(updateUserProfilePicAsync(userData, type))
  };
}

export default connect(mapStateToProps, bindActions)(Settings);
