import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, Easing, TouchableOpacity, Animated, Button, TextInput, Dimensions, Image } from 'react-native';
import { Header, Left, Right, Body, Content } from "native-base";
import Ionicon from "react-native-vector-icons/Ionicons";
import Ficon from "react-native-vector-icons/Feather";
import FAicon from "react-native-vector-icons/FontAwesome";
import Eicon from "react-native-vector-icons/Entypo";
import styles from "./styles";
import { Actions } from "react-native-router-flux";
import platform from '../../../../native-base-theme/variables/platform';
import { connect } from 'react-redux';
import config from '../../../../config';
import { changePageStatus } from "../../../actions/driver/home";
import { startScheduleTrip, startTrip } from "../../../services/driversocket";
import { responseByDriver } from "../../../actions/driver/rideRequest";
import navigate from '../../../utils/navigate';


class ScheduleRide extends Component {
    state = {
        animation: new Animated.Value(100),
        allTrips: [],
        enteredOtpValue: null,
        srcLocLat: null,
        srcLocLon: null,
        DesLocLat: null,
        DesLocLon: null
    }
    startAnimation = () => {
        Animated.timing(this.state.animation, {
            toValue: 200,
            duration: 1000,
            easing: Easing.elastic(5),
        }).start();
    }

    getAllTrips() {
        fetch(`${config.serverSideUrl}:${config.port}/api/trips/getMyRides`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: this.props.jwtAccessToken,
                user: {
                    userId: this.props.userId,
                    userType: 'driver'
                }
            }
        })
            .then(resp => resp.json())
            .then(res => {
                this.setState({ allTrips: res.data })
            })
    }

    componentWillMount() {
        this.getAllTrips()
    }

    onGoToPickupLocClick(tripObj) {
        const srcLocLat = tripObj.srcLoc[0];
        const srcLocLon = tripObj.srcLoc[1];
        const DesLocLat = tripObj.destLoc[0];
        const DesLocLon = tripObj.destLoc[1];
        this.setState({
            srcLocLat: srcLocLat,
            srcLocLon: srcLocLon,
            DesLocLat: DesLocLat,
            DesLocLon: DesLocLon
        })
        navigateData = {
            source: {
                latitude: this.state.srcLocLat,
                longitude: this.state.srcLocLon
            },
            destination: {
                latitude: this.state.DesLocLat,
                longitude: this.state.DesLocLon
            },
            params: [
                {
                    key: 'dirflg'
                }
            ]
        }
        navigate(navigateData);
    }
    onArrivedAtPickUpLocClick(tripObj) {
        if (this.state.enteredOtpValue == tripObj.otp) {
            fetch(`${config.serverSideUrl}:${config.port}/api/trips/getMyRides`, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: this.props.jwtAccessToken,
                },
                body: JSON.stringify({ tripId: tripObj._id, changeTripStatus: 'schedule-enRoute' })
            })
                .then(resp => resp.json())
                .then(res => {
                    this.getAllTrips();
                    this.props.startTrip(tripObj)
                    Actions.pop();

                })
        } else {
            alert("Enter a vaild OTP")
        }

    }

    render() {
        
        const animatedStyles = {
            height: this.state.animation,
        }
        return (
            <View style={styles.container}>
                <Header
                    androidStatusBarColor="#FFB600"
                    iosBarStyle="light-content"
                    style={{ width: '100%', backgroundColor: '#FFB600' }}>
                    <Left style={{ flex: 1 }}>
                        <Ionicon name="md-arrow-back"
                            onPress={() => Actions.pop()}
                            style={styles.backIcon} />
                    </Left>
                    <Body style={{ flex: 8 }}>
                        <Text style={styles.heading}>Schedule Rides</Text>
                    </Body>
                    <Right style={{ flex: 1 }} />
                </Header>
                <Content style={{
                    flex: 9,
                    backgroundColor: '#fff',
                    width: '100%',
                    padding: 10
                }}>{
                        this.state.allTrips.length === 0 || null || undefined ? (
                            <View>
                                <Text style={styles.cardTitle}>You have no scheduled trips</Text>
                                <Image source={require('../../../../assets/images/scheduleTrip.png')}
                                    style={{
                                        width: Dimensions.get("screen").width,
                                        height: 300,
                                        resizeMode: "contain"
                                    }} />
                                <Button
                                    onPress={() => this.getAllTrips()}
                                    title="Refresh" />
                            </View>
                        ) :
                            (
                                this.state.allTrips.map((trip, index) => {
                                    return (
                                        < TouchableOpacity 
                                        style={{justifyContent:'center', alignItems:'center'}}
                                        onPress={this.startAnimation}>
                                            <View style={[styles.detailBox]}>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                                                    <Image
                                                        source={require('../../../../assets/images/Scab.png')}
                                                        style={{ width: 50, height: 50, resizeMode: 'contain' }} />
                                                    <Eicon name="dots-three-horizontal" style={{ color: 'green' }} />
                                                    <Eicon name="dots-three-horizontal" style={{ color: 'red' }} />
                                                    <Eicon name="dots-three-horizontal" style={{ color: 'green' }} />
                                                    <Eicon name="dots-three-horizontal" style={{ color: 'red' }} />
                                                    <Image
                                                        source={require('../../../../assets/images/Sflag.png')}
                                                        style={{ width: 30, height: 30, resizeMode: 'contain' }} />
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <View style={{ flex: 1, justifyContent: 'space-around', alignItems: 'center' }}>
                                                        <FAicon name="dot-circle-o" style={{ color: 'green' }} />
                                                        <Eicon name="dots-three-vertical" style={{ color: 'green' }} />
                                                        <FAicon name="dot-circle-o" style={{ color: 'red' }} />
                                                    </View>
                                                    <View style={{ flex: 9 }}>
                                                        <Text style={styles.cardTitle} numberOfLines={2}>Source :
                                             <Text style={styles.cardSubTitle}> {trip.pickUpAddress}</Text>
                                                        </Text>
                                                        <Text style={styles.cardTitle} numberOfLines={2}>Destination :
                                             <Text style={styles.cardSubTitle}> {trip.destAddress}</Text>
                                                        </Text>
                                                    </View>
                                                </View>
                                                <View style={{ paddingLeft: 10, flexDirection: 'row' }}>
                                                    <View style={{ flex: 1 }}>
                                                        <Text style={styles.cardTitle}>Scheduled Date </Text>
                                                        <Text style={styles.cardTitle}>Scheduled Time </Text>
                                                        <Text style={styles.cardTitle}>Rider Name </Text>
                                                    </View>
                                                    <View style={{ flex: 1.5 }}>
                                                        <Text style={styles.cardSubTitle}>: {trip.scheduleTripDetails.scheduleTripDateTime.slice(0, 10)} </Text>
                                                        <Text style={styles.cardSubTitle}>: {trip.scheduleTripDetails.scheduleTripDateTime.slice(11, 19)} </Text>
                                                        <Text style={styles.cardSubTitle}>: {trip.rider.fname}</Text>
                                                    </View>
                                                </View>
                                                <View style={{
                                                    justifyContent: 'center',
                                                    alignItems: 'center'
                                                }}>
                                                    <TextInput
                                                        style={{
                                                            height: 40,
                                                            width: "70%",
                                                            marginBottom: 20,
                                                            borderRadius: 20,
                                                            marginTop: 20,
                                                            borderColor: 'gray',
                                                            justifyContent: 'center',
                                                            alignItems: 'center',
                                                            borderWidth: 1
                                                        }}
                                                        underlineColorAndroid="transparent"
                                                        keyboardType="numeric"
                                                        textAlign={'center'}
                                                        placeholder="Enter Your 6 Digit Otp"
                                                        maxLength={6}
                                                        onChangeText={(text) => this.setState({ enteredOtpValue: text })} />
                                                </View>

                                                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                                    <Button
                                                        onPress={() => this.onGoToPickupLocClick(trip)}
                                                        title="Go To Pickup" />
                                                    <Button
                                                        onPress={() => this.onArrivedAtPickUpLocClick(trip)}
                                                        title="Start Trip" />
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    );
                                }
                                )
                            )
                    }

                </Content>
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        jwtAccessToken: state.driver.appState.jwtAccessToken,
        userId: state.driver.user._id
    }
}
function bindActions(dispatch) {
    return {
        updateTripStatus: tripObj => dispatch({ type: 'TRIP_REQUEST_UPDATED', payload: tripObj }),
        responseByDriver: response => dispatch(responseByDriver(response)),
        changePageStatus: (newPage) => dispatch(changePageStatus(newPage)),
        startTrip: (tripObj) => dispatch(startTrip(tripObj))
    };
}
export default connect(mapStateToProps, bindActions)(ScheduleRide);
