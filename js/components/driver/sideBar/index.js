import React, { Component } from "react";
import { connect } from "react-redux";
import { Platform, Dimensions } from "react-native";
import PropTypes from "prop-types";
import {
  Content,
  View,
  Text,
  Icon,
  Card,
  CardItem,
  Thumbnail,
  Item,
  List,
  ListItem,
  Left
} from "native-base";
import { Actions, ActionConst } from "react-native-router-flux";
import Iicon from "react-native-vector-icons/Ionicons";
import EIcon from 'react-native-vector-icons/Entypo';
import FIcon from 'react-native-vector-icons/Foundation';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { ImagePicker } from "expo";
import { closeDrawer } from "../../../actions/drawer";
import { logOutUserAsync } from "../../../actions/common/signin";

import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
const deviceHeight = Dimensions.get("window").height;

function mapStateToProps(state) {
  return {
    fname: state.driver.user.fname,
    lname: state.driver.user.lname,
    email: state.driver.user.email,
    jwtAccessToken: state.driver.appState.jwtAccessToken,
    profileUrl: state.driver.user.profileUrl
  };
}
class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null
    };
  }

  static propTypes = {
    fname: PropTypes.string,
    logOutUserAsync: PropTypes.func,
    openDrawer: PropTypes.func,
    closeDrawer: PropTypes.func
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.fname === undefined) {
      Actions.login({ type: ActionConst.RESET });
    }
  }

  handleLogOut() {
    this.props.logOutUserAsync(this.props.jwtAccessToken);
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#FFB600' }}>
        <Content
          bounces={false}
          scrollEnabled={true}
          showsVerticalScrollIndicator={false}
          style={
            Platform.OS === "android"
              ? styles.adrawerContent
              : styles.drawerContent
          }
        >
          <Card
            style={{
              borderColor: "#FFB600",
              marginBottom: 0,
              marginLeft: 0,
              marginRight: 0,
              flexWrap: "nowrap",
              marginTop: 0,
              backgroundColor: "#FFB600",
              flex: 1
            }}
          >
            <CardItem
              style={{
                borderColor: "transparent",
                borderWidth: 0,
                flexDirection: "column",
                backgroundColor: "#FFB600",
                alignItems: "center",
                justifyContent: "center",
                height: deviceHeight / 3 - 26
              }}
            >
              <Item
                style={{
                  borderBottomWidth: 0,
                  borderBottomColor: "transparent"
                }}
              >
                <Thumbnail
                  source={{ uri: this.props.profileUrl }}
                  style={{
                    width: 90,
                    height: 90,
                    borderRadius: 45,
                    borderWidth: 0,
                    borderColor: "transparent"
                  }}
                />
              </Item>
              <Text
                style={{
                  color: "#fff",
                  fontSize: 20,
                  fontWeight: "bold",
                  paddingVertical: 5
                }}
              >
                {_.get(this.props, "fname", "")}{" "}
                {_.get(this.props, "lname", "")}
              </Text>
              <Text
                note
                style={{ color: "#ddd", paddingVertical: 5, fontWeight: "400" }}
              >
                {_.get(this.props, "email", "")}
              </Text>
            </CardItem>
          </Card>
          <List foregroundColor={"black"} style={styles.Bg}>
            <ListItem
              button
              onPress={() => {
                Actions.earnings();
                this.props.closeDrawer();
              }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>
                <Icon
                  name="ios-card"
                  style={[Platform.OS === "ios" ? styles.iosSidebarIcons : styles.aSidebarIcons, { color: 'green', flex: 1 }]}

                />
                <Text style={styles.linkText}>Payment</Text>
              </Left>
            </ListItem>
            <ListItem
              button
              onPress={() => {
                Actions.bookRide();
                this.props.closeDrawer();
              }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>
                <Iicon
                  name="md-timer"
                  style={
                    [Platform.OS === "ios" ? styles.iosSidebarIcons : styles.aSidebarIcons, { color: 'orange', flex: 1 }]
                  }
                />
                <Text style={styles.linkText}>Book Advance Ride</Text>
              </Left>
            </ListItem>
            <ListItem
              button
              onPress={() => {
                Actions.bookedRide();
                this.props.closeDrawer();
              }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>
                <MIcon
                  name="timetable"
                  style={
                    [Platform.OS === "ios" ? styles.iosSidebarIcons : styles.aSidebarIcons, { color: '#dd99ff', flex: 1 }]
                  }
                />
                <Text style={styles.linkText}>My Advance Ride</Text>
              </Left>
            </ListItem>
            <ListItem
              button
              onPress={() => {
                Actions.history();
                this.props.closeDrawer();
              }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>
                <Icon
                  name="ios-keypad-outline"
                  style={
                    [Platform.OS === "ios" ? styles.iosSidebarIcons : styles.aSidebarIcons, { color: 'blue', flex: 1 }]
                  }
                />
                <Text style={styles.linkText}>Trips</Text>
              </Left>
            </ListItem>
            {/*
            <ListItem
              button
              onPress={() => {
                Actions.notifications();
                this.props.closeDrawer();
              }}
              iconLeft
              style={Platform.OS === 'android' ? styles.alinks : styles.links}
            >
              <Left>
                <Icon
                  name="ios-notifications"
                  style={
                    Platform.OS === 'ios'
                      ? styles.iosSidebarIcons
                      : styles.aSidebarIcons
                  }
                />
                <Text style={styles.linkText}>Notifications</Text>
              </Left>
            </ListItem>
          */}
            <ListItem
              button
              onPress={() => {
                Actions.settings();
                this.props.closeDrawer();
              }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>
                <Icon
                  name="ios-settings"
                  style={[Platform.OS === "ios" ? styles.iosSidebarIcons : styles.aSidebarIcons, { color: 'gray', flex: 1 }]}
                />
                <Text style={styles.linkText}>Settings</Text>
              </Left>
            </ListItem>
            <ListItem
              button
              onPress={() => {
                this.props.closeDrawer();
                Actions.AboutUs();
              }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>
                <FIcon
                  name="clipboard-notes"
                  style={
                    [Platform.OS === 'ios' ? (
                      styles.iosSidebarIcons
                    ) : (
                        styles.aSidebarIcons
                      ), { color: 'lightblue', flex: 1 }]
                  }
                />
                <Text style={{ ...styles.linkText, fontWeight: "700" }}>
                  About Us
                </Text>
              </Left>
            </ListItem>
            <ListItem
              button
              onPress={() => {
                this.props.closeDrawer();
                Actions.ContactUs();
              }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>
                <EIcon
                  name="old-phone"
                  style={
                    [Platform.OS === 'ios' ? (
                      styles.iosSidebarIcons
                    ) : (
                        styles.aSidebarIcons
                      ), { color: 'lightgreen', flex: 1 }]
                  }
                />
                <Text style={{ ...styles.linkText, fontWeight: "700" }}>
                  Contact Us
                </Text>
              </Left>
            </ListItem>
            <ListItem
              button
              onPress={() => {
                this.props.closeDrawer();
                this.handleLogOut();
              }}
              iconLeft
              style={Platform.OS === "android" ? styles.alinks : styles.links}
            >
              <Left>
                <Icon
                  name="ios-power"
                  style={[Platform.OS === "ios" ? styles.iosSidebarIcons : styles.aSidebarIcons, { color: 'red', flex: 1 }]}
                />
                <Text style={{ ...styles.linkText, fontWeight: "700" }}>
                  Sign Out
                </Text>
              </Left>
            </ListItem>
          </List>
        </Content>
      </View>
    );
  }
}

function bindAction(dispatch) {
  return {
    closeDrawer: () => dispatch(closeDrawer()),
    logOutUserAsync: jwtAccessToken => dispatch(logOutUserAsync(jwtAccessToken))
  };
}

export default connect(mapStateToProps, bindAction)(SideBar);
