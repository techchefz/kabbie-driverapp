//import liraries
import React, { Component } from 'react';
import { View, Platform, Dimensions, TouchableOpacity, Image, SafeAreaView, StyleSheet } from 'react-native'
import {
    Container,
    Header,
    Content,
    Text,
    Button,
    Icon,
    Thumbnail,
    Card,
    CardItem,
    Title,
    Left,
    Right,
    Body,
    Label,
    Input,
    Item,
    Form
} from "native-base";
import styles from "./styles";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import config from "../../../../config";
import { transferRide } from "../../../actions/driver/transfer";
function mapStateToProps(state) {
    return {
        jwtAccessToken: state.driver.appState.jwtAccessToken,
        _id: state.driver.user._id,
    };
}
// create a component
class BookedRide extends Component {
    constructor(props) {
        super(props)
        this.state = {
            RideCards: [],
            rideStart: false,
        }
    }

    componentWillMount() {
        // console.log('====================================');
        // console.log(this.props._id);
        // console.log('====================================');
        fetch(`${config.serverSideUrl}:${config.port}/api/self-ride/getAllSelfRide`,
            {
                method: 'GET',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: this.props.jwtAccessToken,
                    user_id: this.props._id,
                },
            })
            .then(resp => resp.json()).then((data) => {
                // console.log('====================================');
                // console.log(data.data);
                // console.log('====================================');
                this.setState({ RideCards: data.data })
            })
    }
    onTransfer(data) {
        this.props.transferRide(data),
        Actions.TransferRide()
    }
    onStart(data){
        data.tripRequestStatus = "started"
        fetch(`${config.serverSideUrl}:${config.port}/api/self-ride/rideStatusUpdate`,
        {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                Authorization: this.props.jwtAccessToken
            },
            body:JSON.stringify({
                data
            })
        })
        .then(resp => resp.json()).then((data) => {
            // console.log('====================================');
            // console.log(data.data);
            // console.log('====================================');
            this.setState({ RideCards: data.data })
        })
    }
    onComplete(data){
        data.tripRequestStatus = "end"
        fetch(`${config.serverSideUrl}:${config.port}/api/self-ride/rideStatusUpdate`,
        {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                Authorization: this.props.jwtAccessToken
            },
            body:JSON.stringify({
                data
            })
        })
        .then(resp => resp.json()).then((data) => {
            // console.log('====================================');
            // console.log(data.data);
            // console.log('====================================');
            this.setState({ RideCards: data.data })
        })
    }
    render() {
        return (
            <View style={styles.container}>
                <Header
                    height={45}
                    iosBarStyle="dark-content"
                    androidStatusBarColor="#FFB600"
                    style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
                >
                    <Left>
                        <Button transparent onPress={() => Actions.pop()}>
                            <Icon
                                name="md-arrow-back"
                                style={{ fontSize: 28, color: "#000" }}
                            />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={Platform.OS === "ios" ? styles.iosHeaderTitle : styles.aHeaderTitle}>
                            Book Ride
            </Title>
                    </Body>
                    <Right />
                </Header>
                <Content style={{ width: "100%", flex: 1, padding: 10, }}>
                    {this.state.RideCards.map((data, i) =>
                    data.isTransfered == false ? 
                        <View
                            key={i}
                            style={styles.card}>
                            {data.tripRequestStatus == "end" ?
                            <Image source={require("../../../../assets/images/completed.png")} 
                            style={{width:100, height:100, resizeMode:"contain" ,position:"absolute", top:10, alignSelf:"center", opacity:0.3}}/> : null }
                            <Text style={styles.label}>Booking ID : <Text> {data._id}</Text></Text>
                            <Text style={styles.label}>Rider Name : <Text> {data.riderName}</Text></Text>
                            <Text style={styles.label}>Pickup Location : <Text> {data.pickUpAddress}</Text></Text>
                            <Text style={styles.label}>Drop Location : <Text> {data.destAddress}</Text></Text>
                            {data.tripRequestStatus == "pending" ? 
                        <View style={{ width: "100%", marginTop: 30, justifyContent: "space-around", alignItems: "center", flexDirection: 'row' }}>
                        <Button
                            onPress={() => this.onTransfer(data)}
                            style={{ alignSelf: "center", width: "40%", elevation: 4, backgroundColor:"#66b3ff" }}
                            success>
                            <Text style={{ textAlign: 'center', width: "100%" }}>Transfer</Text>
                        </Button>
                        <Button
                            onPress={() => this.onStart(data)}
                            style={{ alignSelf: "center", width: "40%", elevation: 4 }}
                            success>
                            <Text style={{ textAlign: 'center', width: "100%" }}>Lets Go</Text>
                        </Button>
                    </View> :
                    data.tripRequestStatus == "started" ?
                    <View style={{ width: "100%", marginTop: 30, justifyContent: "space-around", alignItems: "center", flexDirection: 'row' }}>
                               
                                <Button
                                    onPress={() => this.onComplete(data)}
                                    style={{ alignSelf: "center", width: "60%", elevation: 4 }}
                                    success>
                                    <Text style={{ textAlign: 'center', width: "100%" }}>Complete Ride</Text>
                                </Button>
                            </View>    : 
                            null
                        }
                            
                        </View> : null
                    )}
                    <View style={{ width: '100%', height: 30 }} />
                </Content>
            </View>
        );
    }
}

function bindActions(dispatch) {
    return {
        transferRide: data => dispatch(transferRide(data)),
    };
}
//make this component available to the app
export default connect(mapStateToProps, bindActions)(BookedRide);
