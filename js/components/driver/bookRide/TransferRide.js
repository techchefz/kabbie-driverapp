//import liraries
import React, { Component } from 'react';
import { View, Platform, Dimensions, TouchableOpacity, Image, SafeAreaView, StyleSheet, Picker } from 'react-native'
import {
    Header,
    Content,
    Text,
    Button,
    Icon,
    Title,
    Left,
    Right,
    Body,
    Form,
    Toast
} from "native-base";
import styles from "./styles";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import config from "../../../../config";
import { duration } from 'moment';

function mapStateToProps(state) {
    return {
        jwtAccessToken: state.driver.appState.jwtAccessToken,
        transferedRide: state.driver.trip.transferData
    };
}
// create a component
class TransferRide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            drivers: [],
            selected2: undefined,
            driverDetails: {}
        }
    }
    onValueChange2(value: string) {

        this.setState({
            driverDetails: value,
            selected2: value.fname
        });
    }
    componentWillMount() {
        fetch(`${config.serverSideUrl}:${config.port}/api/self-ride/getAllDrivers`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                Authorization: this.props.jwtAccessToken,
                user_id: this.props._id,
            },
        }).then((res) => res.json()).then((result) => {
            this.setState({ drivers: result.data })
        })
    }
    onTransfer(driver) {
        fetch(`${config.serverSideUrl}:${config.port}/api/self-ride/transferSelfRide`,
            {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: this.props.jwtAccessToken,
                },
                body: JSON.stringify({
                    tripId: this.props.transferedRide._id,
                    driverId: driver._id,
                })
            })
            .then(resp => resp.json()).then((data) => {
                console.log('=============datadata=======================');
                console.log(data);
                console.log('==============datadata======================');
                if (data.success == true) {
                    Toast.show({
                        text: "Ride transfered successfully",
                        buttonText: "Okay",
                        type: "success",
                        duration:10000
                      })
                    Actions.bookRide()
                } else {
                    alert("internal server error")
                }
            })
    }
    render() {
        return (
            <View style={styles.container}>
                <Header
                    height={45}
                    iosBarStyle="dark-content"
                    androidStatusBarColor="#FFB600"
                    style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
                >
                    <Left>
                        <Button transparent onPress={() => Actions.pop()}>
                            <Icon
                                name="md-arrow-back"
                                style={{ fontSize: 28, color: "#000" }}
                            />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={Platform.OS === "ios" ? styles.iosHeaderTitle : styles.aHeaderTitle}>
                            Transfer Ride
            </Title>
                    </Body>
                    <Right />
                </Header>
                <Content style={{ flex: 1, padding: 10, width: "100%" }}>
                    <View style={{
                        backgroundColor: "white",
                        padding: 10,
                        width: "100%",
                        ...Platform.select({
                            android: { elevation: 4 },
                            ios: {
                                shadowColor: 'black',
                                shadowOffset: { height: 1, width: 1 },
                                shadowOpacity: 0.3,
                                shadowRadius: 2,
                            }
                        }),
                    }}>
                        <Text>BookingID: {this.props.transferedRide._id}{'\n'}</Text>
                        <Text>Rider Name: {this.props.transferedRide.riderName}{'\n'}</Text>
                        <Text>Rider PhoneNo: {this.props.transferedRide.riderPhoneNo}{'\n'}</Text>
                    </View>
                    <View style={{
                        backgroundColor: "white",
                        padding: 10,
                        width: "100%",
                        marginTop: 20,
                        ...Platform.select({
                            android: { elevation: 4 },
                            ios: {
                                shadowColor: 'black',
                                shadowOffset: { height: 1, width: 1 },
                                shadowOpacity: 0.3,
                                shadowRadius: 2,
                            }
                        }),
                    }}>
                        <Form>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                placeholder="Select your SIM"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                style={{ width: undefined }}
                                selectedValue={this.state.selected2}
                                onValueChange={this.onValueChange2.bind(this)}
                            >
                                {
                                    this.state.drivers.map((driver, index) => {

                                        return <Picker.Item key={index} label={driver.fname + " " + driver.lname} value={driver} />
                                    })
                                }
                            </Picker>
                        </Form>
                    </View>
                    <View style={{
                        backgroundColor: "white",
                        padding: 10,
                        width: "100%",
                        marginTop: 20,
                        ...Platform.select({
                            android: { elevation: 4 },
                            ios: {
                                shadowColor: 'black',
                                shadowOffset: { height: 1, width: 1 },
                                shadowOpacity: 0.3,
                                shadowRadius: 2,
                            }
                        }),
                    }}>
                        <Text>Driver Name :: {this.state.selected2 == undefined ? "Choose driver" : this.state.selected2}</Text>
                    </View>
                    <View style={{ width: "100%", marginTop: 30, justifyContent: "space-around", alignItems: "center" }}>
                        <Button
                            onPress={() => this.onTransfer(this.state.driverDetails)}
                            style={{ alignSelf: "center", width: "50%", elevation: 4 }}
                            success>
                            <Text style={{ textAlign: 'center', width: "100%" }}>Transfer Ride</Text>
                        </Button>
                    </View>
                </Content>
            </View>
        );
    }
}


//make this component available to the app
export default connect(mapStateToProps)(TransferRide);

