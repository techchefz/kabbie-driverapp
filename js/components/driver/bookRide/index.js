import React, { Component } from 'react'
import { View, Platform, Dimensions, TouchableOpacity, Image, SafeAreaView } from 'react-native'
import {
    Container,
    Header,
    Content,
    Text,
    Button,
    Icon,
    Thumbnail,
    Card,
    CardItem,
    Title,
    Left,
    Right,
    Body,
    Label,
    Input,
    Item,
    Form
} from "native-base";
import styles from "./styles";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import config from "../../../../config";
import DatePicker from 'react-native-datepicker';

function mapStateToProps(state) {
    return {
        jwtAccessToken: state.driver.appState.jwtAccessToken,
        _id: state.driver.user._id,
    };
}
export class BookRide extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: null,
            phoneNo: null,
            dropLocation: null,
            pickupLocation: null,
            date: null,
            minDate: new Date(),
        }
    }



    onBookRide() {
        if (this.state.name == null && 
            this.state.phoneNo == null && 
            this.state.pickupLocation == null && 
            this.state.dropLocation == null && 
            this.state.date == null) {
            alert("all fields are mendetory")
        } else {
            fetch(`${config.serverSideUrl}:${config.port}/api/self-ride/selfRideRequest`,
            {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: this.props.jwtAccessToken,
                },
                body: JSON.stringify({
                    driverId: this.props._id,
                    riderName: this.state.name,
                    riderPhoneNo: this.state.phoneNo,
                    pickUpAddress: this.state.pickupLocation,
                    destAddress: this.state.dropLocation,
                    datetime: this.state.date
                })
            })
            .then(resp => resp.json()).then((data) => {
                console.log('=================Data===================')
                console.log(data.success);
                console.log('=================Data===================')
                Actions.bookedRide();
            })
        }
        
    }
    render() {

        return (
            <View>
                <SafeAreaView style={{ backgroundColor: "#FFB600", width: "100%" }} />
                <Header
                    height={45}
                    iosBarStyle="dark-content"
                    androidStatusBarColor="#FFB600"
                    style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
                >
                    <Left>
                        <Button transparent onPress={() => Actions.driverStartupService()}>
                            <Icon
                                name="md-arrow-back"
                                style={{ fontSize: 28, color: "#000" }}
                            />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={Platform.OS === "ios" ? styles.iosHeaderTitle : styles.aHeaderTitle}>
                            Book Ride
            </Title>
                    </Body>
                    <Right />
                </Header>
                <View style={{ height: Dimensions.get("screen").height, backgroundColor: 'white' }}>
                    <Content>
                        <View style={{
                            width: "100%",
                            padding: 20
                        }}>
                            {/* <View
                                style={Platform.OS === "ios" ? styles.iosSrcdes : styles.aSrcdes}
                            >
                                <View style={styles.searchBar}>
                                    <View>
                                        <Item
                                            regular
                                            style={{
                                                backgroundColor: "#FFF",
                                                borderWidth: 0,
                                                marginLeft: 0,
                                                borderColor: "transparent"
                                            }}
                                        >
                                            <Icon name="ios-search" style={styles.searchIcon} />
                                            <Button
                                                onPress={() => {
                                                    Actions.suggestLocation({
                                                        heading: "Starting Location",
                                                        page: "home"
                                                    });
                                                }}
                                                transparent
                                                style={{ flex: 1, paddingLeft: 10, fontSize: 16 }}
                                            >
                                                <Text multiline={Platform.OS !== "ios"} numberOfLines={1}>
                                                    {_.get(this.props, "pickUpAddress", "Pickup Location")}
                                                </Text>
                                            </Button>
                                        </Item>
                                    </View>
                                </View>
                                <View style={styles.searchBar}>
                                    <View>
                                        <Item
                                            regular
                                            style={{
                                                backgroundColor: "#FFF",
                                                marginLeft: 0,
                                                borderColor: "transparent"
                                            }}
                                        >
                                            <Icon name="ios-search" style={styles.searchIcon} />
                                            <Button
                                                onPress={() => {
                                                    Actions.suggestLocation({
                                                        heading: "Destination Location",
                                                        page: "destination"
                                                    });
                                                }}
                                                transparent
                                                style={{ flex: 1, paddingLeft: 10, fontSize: 16 }}
                                            >
                                                {this.props.destAddress !== "" ? (
                                                    <Text multiline={Platform.OS !== "ios"} numberOfLines={1}>
                                                        {_.get(
                                                            this.props,
                                                            "destAddress",
                                                            "Drop Location"
                                                        )}
                                                    </Text>
                                                ) : (
                                                        <Text multiline={Platform.OS !== "ios"} numberOfLines={1}>
                                                            Enter Drop Location </Text>
                                                    )}
                                            </Button>
                                        </Item>
                                    </View>
                                </View>
                            </View> */}
                            <Form>
                                <Item floatingLabel last>
                                    <Label>Rider Name</Label>
                                    <Input
                                        onChangeText={(name) => this.setState({ name })} />
                                </Item>
                                <Item floatingLabel last>
                                    <Label>Rider Phone</Label>
                                    <Input
                                        maxLength={10}
                                        keyboardType="number-pad"
                                        onChangeText={(phoneNo) => this.setState({ phoneNo })} />
                                </Item>
                                <Item floatingLabel last>
                                    <Label>Drop Location</Label>
                                    <Input onChangeText={(dropLocation) => this.setState({ dropLocation })} />
                                </Item>
                                <Item floatingLabel last>
                                    <Label>Pickup Location</Label>
                                    <Input onChangeText={(pickupLocation) => this.setState({ pickupLocation })} />
                                </Item>
                                {/* <Item floatingLabel last> */}
                                <View style={{ width: "100%", marginTop: 30 }}>
                                    <Text style={{ marginBottom: 20, color: "#666666" }}>Choose Pickup Date {'&'} Time</Text>
                                    <DatePicker
                                        style={{
                                            width: "100%",
                                            marginBottom: 20,
                                        }}
                                        date={this.state.date}
                                        mode="datetime"
                                        minDate={this.state.minDate}
                                        format="YYYY-MM-DD HH:mm"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        showIcon={false}
                                        onDateChange={(date) => {this.setState({date: date})}}
                                    />
                                    {/* </Item> */}
                                </View>
                            </Form>
                            <View style={{ width: "100%", marginTop: 30, justifyContent: "center", alignItems: "center" }}>
                                <Button
                                    onPress={() => this.onBookRide()}
                                    style={{ alignSelf: "center", width: 200 }}
                                    success>
                                    <Text style={{ textAlign: 'center', width: "100%" }}>Book Ride</Text>
                                </Button>
                            </View>
                        </View>
                    </Content>
                </View>
            </View>
        )
    }
}

export default connect(mapStateToProps)(BookRide);