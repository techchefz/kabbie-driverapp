import commonColor from '../../../../native-base-theme/variables/commonColor';

const React = require('react-native');

const {Dimensions, Platform} = React;

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

export default {
  iosHeader: {
    backgroundColor: '#FFB600',
    width:"100%"
  },
  aHeader: {
    backgroundColor: '#FFB600',
    borderColor: '#aaa',
    elevation: 3,
    width:"100%"
  },
  iosHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    color: "#000",
  },
  aHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 26,
    marginTop: -5,
    color: "#000",
  },
  searchBar: {
    width: deviceWidth - 30,
    alignSelf: "center",
    backgroundColor: "transparent",
    borderColor: "#ddd",
    borderWidth: 1,
    marginLeft: -5,
    marginTop: 5,
    shadowColor: "#aaa",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 3,
    shadowOpacity: 0.5
  },
  aSrcdes: {
    paddingVertical: 7,
    backgroundColor: "transparent"
  },
  iosSrcdes: {
    paddingVertical: 7,
    backgroundColor: "transparent"
  },
  container: {
    flex: 1,
    width:"100%",
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f2f2f2',
},
label: {
  marginBottom: 3,
  color:"#666666",
  backgroundColor:"transparent"
}, 
card: {
  width: "100%",
  borderRadius: 10,
  padding: 15,
  elevation:8,
  backgroundColor:"white",
  marginBottom:10,
  ...Platform.select({
    android: { elevation: 4 ,
    borderWidth:0.5,
  borderColor:'#e4e4e4'},
    ios: {
      shadowColor: 'black',
      shadowOffset: { height: 1, width: 1 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
    }
  }),
}
};
