import commonColor from '../../../../native-base-theme/variables/commonColor';

const React = require('react-native');
import { Platform} from "react-native";


const {Dimensions} = React;

const deviceHeight = Dimensions.get('window').height;

export default {
  iosHeader: {
    backgroundColor: '#FFB600',
  },
  aHeader: {
    backgroundColor: '#FFB600',
    borderColor: '#aaa',
    elevation: 3,
  },
  iosHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    color: "#fff",
  },
  aHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 26,
    marginTop: -5,
    color: "#fff",
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
},
heading: {
    color: 'white',
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontSize: 20,
    // marginTop:30
},
tap: {
    color:"#fff",
    backgroundColor:'transparent',
    marginTop:20,
},
msg: {
    textAlign:'center',
    backgroundColor:'transparent',
    color: 'white',
    width:'70%',
    marginTop:30
},
shareView: {
    backgroundColor:'#fff',
    width:'60%',
    height:100,
    flexDirection:'row',
    justifyContent: "space-around",
    alignItems:'center',
    marginTop:10,
    borderRadius:5
},
whatsapp: {
    fontSize:80,
    color:'#fff',
    backgroundColor:'transparent'
},
verticalLine: {
    borderRightWidth:0.5,
    borderColor:'gray',
    height:'60%'
},
horizontelLine: {
    borderTopWidth:0.5,
    borderColor:"gray",
    width:'80%',
    marginTop:10
},
backView1: {
    // paddingTop:20,
    // height: Dimensions.get('screen').height,
    height:'100%',
    width: Dimensions.get('screen').width,
    alignItems:'center'
},
backIcon: {
    // position:'absolute',
    // top:30,
    // left:10,
    backgroundColor:'transparent',
    fontSize:30,
    color:'#fff'
},
boxContainer: {
    marginTop:50,
    height:50,
    width:'90%'
},
boxBorder: {
    marginTop:10,
    borderWidth:1,
    borderColor:'white',
    width:'100%',
    height:20,
    borderRadius:10
},
boxFilling: {
    borderTopLeftRadius:10,
    borderBottomLeftRadius:10,
    backgroundColor:'orange',
    height:'100%'
},
subHeading: {
    marginTop: 10,
    marginBottom:10,
    color: "#fff",
    fontSize:16,
    backgroundColor:'transparent'
},
referBox: {
    width:'80%',
    height:50,
    borderWidth:1.5,
    borderStyle:'dashed',
    borderRadius:10,
    borderColor:'#fff',
    marginTop:30,
    justifyContent:'center',
    alignItems:'center'
},
referCode: {
    fontSize:20,
    color:'#fff',
    backgroundColor: 'transparent'
},
shareButton: {
    width:100,
    height:30,
    // borderWidth:0.5,
    borderRadius:5,
    backgroundColor:'#ffe4b3',
    justifyContent:'center',
    alignItems:'center',
    marginTop:20,
    ...Platform.select({
        android: {elevation: 2},
        ios : {
            shadowColor:'black',
            shadowRadius:2,
            shadowOffset: {width:2, height:2},
            shadowOpacity:0.5
        }
    })
},
instruction: {
    color:'#fff',
    backgroundColor:'transparent',
    textAlign:"left",
    width:'90%'
},
shareText: {
    color:'#000',
    backgroundColor:'transparent',
    fontWeight:'bold'
}
};
