import React, { Component } from 'react'
import { View, Platform, Dimensions, Image, TouchableOpacity } from 'react-native'
import {
    Container,
    Header,
    Content,
    Text,
    Button,
    Icon,
    Thumbnail,
    Card,
    CardItem,
    Title,
    Left,
    Right,
    Body
} from "native-base";
import styles from "./styles";
import { Actions } from "react-native-router-flux";
import Ionicon from "react-native-vector-icons/Ionicons";
import MIcon from "react-native-vector-icons/MaterialIcons";
import EIcon from "react-native-vector-icons/Entypo";
import FIcon from "react-native-vector-icons/FontAwesome";

export class RefernEarn extends Component {
    render() {
        
        return (
            <View>
                <Header
                    iosBarStyle="light-content"
                    androidStatusBarColor="#FFB600"
                    style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
                >
                    <Left>
                        <Button transparent onPress={() => Actions.pop()}>
                            <Icon
                                name="md-arrow-back"
                                style={{ fontSize: 28, color: "#fff" }}
                            />
                        </Button>
                    </Left>
                    <Body>
                        <Title
                            style={
                                Platform.OS === "ios"
                                    ? styles.iosHeaderTitle
                                    : styles.aHeaderTitle
                            }
                        >
                            Refer n Earn
            </Title>
                    </Body>
                    <Right />
                </Header>
                <View style={{height:Dimensions.get("screen").height, backgroundColor:'white'}}>
                <Content>
                        <View>
                            <Image 
                                source={require("../../../../assets/images/refer.png")}
                                style={{
                                    width:'100%',
                                    height: Dimensions.get("screen").height * (1/3),
                                    resizeMode:"contain"
                                }}/>
                        </View>
                        <Text style={{fontSize:18, color:'black', textAlign:'center', marginTop:25, fontWeight:"100", marginBottom:10}}>Share your referral code</Text>
                        <Text style={{fontSize:22, color:'black', textAlign:'center'}}>6GV5F98</Text>
                        <Text style={{fontSize:18, color:'black', textAlign:'left',marginLeft:10, marginTop:30, fontWeight:"100", textAlign:"center"}}>Start inviting your friends</Text>
                        <View style={{justifyContent:'center', alignItems:'center'}}>
                        <TouchableOpacity 
                        onPress={()=> alert('Referral code successfully shared')}
                        style={{
                            backgroundColor:'#FFB600',
                            width:150,
                            height:50,
                            borderRadius:5,
                            justifyContent:'center',
                            alignItems:'center',
                            marginTop:30
                        }}>
                        <Text style={{fontSize:22, color:'white', textAlign:'center', backgroundColor:'transparent',}}>SHARE</Text>
                        </TouchableOpacity>
                        </View>
                </Content>
                </View>
            </View>
        )
    }
}

export default RefernEarn