import React, { Component } from 'react'
import { View, Platform, Dimensions, TouchableOpacity, Image, SafeAreaView } from 'react-native'
import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Thumbnail,
  Card,
  CardItem,
  Title,
  Left,
  Right,
  Body
} from "native-base";
import styles from "./styles";
import { Actions } from "react-native-router-flux";

export class AboutUs extends Component {
  render() {
    
    return (
      <View>
        <SafeAreaView style={{ backgroundColor: "#FFB600", width: "100%" }} />
        <Header
          height={45}
          iosBarStyle="dark-content"
          androidStatusBarColor="#FFB600"
          style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
        >
          <Left>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon
                name="md-arrow-back"
                style={{ fontSize: 28, color: "#000" }}
              />
            </Button>
          </Left>
          <Body>
            <Title style={Platform.OS === "ios" ? styles.iosHeaderTitle : styles.aHeaderTitle}>
              About Us
            </Title>
          </Body>
          <Right />
        </Header>
        <View style={{ height: Dimensions.get("screen").height, backgroundColor: 'white' }}>
          <Content>
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Image
                source={require("../../../../assets/images/kabbie_logo.png")}
                style={{
                  marginTop:20,
                  width: '80%',
                  height: Dimensions.get("screen").height * (1 / 3),
                  resizeMode: "contain"
                }} />
            </View>
            <Text style={{ fontSize: 18, color: 'black', textAlign: 'center', marginTop: 25, fontWeight: "100", marginBottom: 10 }}>Version 1.0.1</Text>
            <Text
              onPress={() => alert('Terms & conditions')}
              style={{
                textAlign: 'center',
                top: Dimensions.get('screen').height * (1 / 3)
              }}>Terms {"&"} Conditions</Text>

          </Content>
        </View>
      </View>
    )
  }
}

export default AboutUs