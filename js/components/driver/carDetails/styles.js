import commonColor from '../../../../native-base-theme/variables/commonColor';

export default {
  iosHeader: {
    backgroundColor: '#FFB600'
  },
  aHeader: {
    backgroundColor: '#FFB600',
    borderColor: '#aaa',
    elevation: 3
  },
  iosHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    color: '#000'
  },
  aHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 26,
    marginTop: -5,
    color: '#000'
  },
  carType: {
    justifyContent:"center", 
    alignItems:"center" , 
    height:"100%", 
    width:"25%"
    }
};
