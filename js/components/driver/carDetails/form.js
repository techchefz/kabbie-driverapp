import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import DatePicker from "react-native-datepicker";
import { Text, Dimensions, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import {
  Input,
  Button,
  Label,
  Item,
  View,
  ListItem,
  Form,
  Picker
} from "native-base";

import { Actions } from "react-native-router-flux";
import { updateUserFormsDetails } from "../../../actions/driver/settings";
import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";

const { width, height } = Dimensions.get("window");

const validate = values => {
  const errors = {};
  if (!values.company) {
    errors.company = "Company Name is Required";
  } else if (!values.regNo) {
    errors.regNo = "Registration Number is Required";
  } else if (!values.RC_ownerName) {
    errors.RC_ownerName = "Owner Name is Required";
  } else if (!values.vehicleNo) {
    errors.vehicleNo = "Vehicle Number is Required";
  } else if (!values.carModel) {
    errors.carModel = "Car model is Required";
  } else if (!values.type) {
    errors.type = "Please select type of your car";
  }
  return errors;
};

const selectedQuickest = 0;
const selectedSedan = 0;
const selectedLuxury = 0;
const selectedWagon = 0;
const selectedWheelchair = 0;
const selectedMaxi = 0;

export const input = props => {
  const { meta, input } = props;
  return (
    <View style={{ flex: 1, width: null }}>
      <Item>
        <Input
          {...input}
          {...props}
        // style={{ fontWeight: "bold", marginLeft: 2, borderBottomWidth: 1 }}
        />
      </Item>
      {meta.touched &&
        meta.error && <Text style={{ color: "red" }}>{meta.error}</Text>}
    </View>
  );
};
export const regDate = props => {
  const { meta, input: { onChange, value } } = props;
  return (
    <View style={{ flex: 1, width: null }}>
      <Datepickercustom
        onUpdate={updatedDate => onChange(updatedDate)}
        date={value}
      />
    </View>
  );
};

input.propTypes = {
  input: PropTypes.object
};

class Datepickercustom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      carType: null,
      selectedQuickest: 0,
      selectedSedan: 0,
      selectedLuxury: 0,
      selectedWagon: 0,
      selectedWheelchair: 0,
      selectedMaxi: 0,
    };
  }
  state = {

  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          width: null
        }}
      >
        <Item>
          <DatePicker
            style={{
              width: width,
              color: "#000"
            }}
            date={this.props.date}
            mode="date"
            format="MM/DD/YYYY"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            androidMode="default"
            showIcon={false}
            customStyles={{
              dateInput: {
                borderWidth: 0,
                height: null,
                alignItems: "flex-start"
              },
              dateText: {
                color: "#000"
              }
            }}
            onDateChange={date => {
              this.props.onUpdate(date);
            }}
          />
        </Item>
      </View>
    );
  }
}

// const picker = props => {
//   const { input, object, ...inputProps } = props;
//   // if (input.value === '')
//   //   input.value = { name: props.defaultValue.name, id: props.defaultValue.id };
//   return (
//     <ListItem
//       style={{
//         flexDirection: "row",
//         marginLeft: -10,
//         paddingTop: 0,
//         paddingBottom: 0,
//         borderBottomWidth: 1,
//         borderColor: "#000"
//       }}
//     >
//       <Picker
//         {...inputProps}
//         onBlur={() => {}}
//         iosHeader="Select one"
//         mode="dropdown"
//         selectedValue={input.value.name}
//       >
//         <Item label="TaxiPool" value="key0" />
//         <Item label="TaxiGo" value="key1" />
//         <Item label="TaxiXL" value="key2" />
//       </Picker>
//     </ListItem>
//   );
// };
function mapStateToProps(state) {
  return {
    tripRequest: state.user,
  };
}
class CarForm extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    handleSubmit: PropTypes.func
  };
  submit(values) {
    const carDetails = {
      company: values.company,
      regNo: values.regNo,
      RC_ownerName: values.RC_ownerName,
      vehicleNo: values.vehicleNo,
      carModel: values.carModel,
      regDate: values.regDate,
      type: this.state.carType
    };
    const userDetails = { ...this.props.user, carDetails };

    this.props.dispatch(updateUserFormsDetails(userDetails));
    Actions.bankDetails();
  }
  carTypeEconomy() {
    this.props.carDetails.type = "Eco"
    alert("done")
  }
  render() {
    // console.log('===tripRequest=========================tripRequest========')
    // console.log(this.props.tripRequest)
    // console.log('====tripRequest=========================tripRequest=======')
    return (
      <View
        style={{
          flex: 1,
          alignSelf: "stretch",
          justifyContent: "space-around"
        }}
      >
        <Form>
          {/*<View style={{ padding: 10 }}>
            <Field
              component={picker}
              style={{ marginLeft: 10, width: width - 20 }}
              name="type"
              placeholder="Type"
              placeholderTextColor="#000"
            />
      </View>*/}
          <View style={{ padding: 10 }}>
            <Field
              component={input}
              name="company"
              placeholder="Company"
              placeholderTextColor="#000"
            />
          </View>
          <View style={{ padding: 10 }}>
            <Field
              component={input}
              name="regNo"
              placeholder="Registration Number"
              placeholderTextColor="#000"
            />
          </View>
          <View style={{ padding: 10 }}>
            <Field
              component={input}
              name="RC_ownerName"
              placeholder="Owner Name"
              placeholderTextColor="#000"
            />
          </View>
          <View style={{ padding: 10 }}>
            <Field
              component={input}
              name="vehicleNo"
              placeholder="Vehicle Number"
              placeholderTextColor="#000"
            />
          </View>
          <View style={{ padding: 10 }}>
            <Field
              component={input}
              name="carModel"
              placeholder="Car Model"
              placeholderTextColor="#000"
            />
          </View>
          <View style={{ padding: 10 }}>
            <Text
              style={{
                fontWeight: "bold",
                color: "#000"
              }}
            >
              Reg Date
            </Text>
            <Field component={regDate} name="regDate" />
          </View>
        </Form>
        <View style={{ padding: 10 }}>
          <Text style={{ color: "#000", fontWeight: "bold" }}>Select Car Type</Text>
        </View>
        <View style={{
          width: "100%",
          padding: 10,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          height: 50
        }}>
          <TouchableOpacity
            onPress={() => [this.setState({ carType: "Quickest" }),
            this.selectedQuickest = 1,
            this.selectedSedan = 0,
            this.selectedLuxury = 0,
            this.selectedWagon = 0,
            this.selectedWheelchair = 0,
            this.selectedMaxi = 0,
            ]}
            style={[styles.carType, { backgroundColor: commonColor.lightThemePlaceholder, borderWidth: this.selectedQuickest }]}>
            <Text>Quickest</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => [this.setState({ carType: "Sedan" }),
            this.selectedQuickest = 0,
            this.selectedSedan = 1,
            this.selectedLuxury = 0,
            this.selectedWagon = 0,
            this.selectedWheelchair = 0,
            this.selectedMaxi = 0,
            ]}
            style={[styles.carType, { backgroundColor: "lightblue", borderWidth: this.selectedSedan }]}>
            <Text>Sedan</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => [this.setState({ carType: "Luxury" }),
            this.selectedQuickest = 0,
            this.selectedSedan = 0,
            this.selectedLuxury = 1,
            this.selectedWagon = 0,
            this.selectedWheelchair = 0,
            this.selectedMaxi = 0,
            ]}
            style={[styles.carType, { backgroundColor: commonColor.lightThemePlaceholder, borderWidth: this.selectedLuxury }]}>
            <Text>Luxury</Text>
          </TouchableOpacity>
        </View>
        <View style={{
          width: "100%",
          padding: 10,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          height: 50
        }}>
          <TouchableOpacity
            onPress={() => [this.setState({ carType: "Wagon" }),
            this.selectedQuickest = 0,
            this.selectedSedan = 0,
            this.selectedLuxury = 0,
            this.selectedWagon = 1,
            this.selectedWheelchair = 0,
            this.selectedMaxi = 0,
            ]}
            style={[styles.carType, { backgroundColor: "lightblue", borderWidth: this.selectedWagon }]}>
            <Text>Wagon</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => [this.setState({ carType: "Wheelchair" }),
            this.selectedQuickest = 0,
            this.selectedSedan = 0,
            this.selectedLuxury = 0,
            this.selectedWagon = 0,
            this.selectedWheelchair = 1,
            this.selectedMaxi = 0,
            ]}
            style={[styles.carType, { backgroundColor: commonColor.lightThemePlaceholder, borderWidth: this.selectedWheelchair }]}>
            <Text>Wheelchair</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => [this.setState({ carType: "Maxi" }),
            this.selectedQuickest = 0,
            this.selectedSedan = 0,
            this.selectedLuxury = 0,
            this.selectedWagon = 0,
            this.selectedWheelchair = 0,
            this.selectedMaxi = 1,
            ]}
            style={[styles.carType, { backgroundColor: "lightblue", borderWidth: this.selectedMaxi }]}>
            <Text>Maxi</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "space-between",
            marginBottom: 15
          }}
        >
          <Button
            style={{
              flex: 1,
              padding: 10,
              height: 50,
              bottom: 0,
              marginHorizontal: 5,
              marginTop: 50,
              backgroundColor: '#FFB600',
              alignItems: "center",
              justifyContent: "center"
            }}
            onPress={() => Actions.pop()}
          >
            <Text
              style={{
                color: "#000",
                fontWeight: "bold",
                fontSize: 18
              }}
            >
              {" "}
              Back{" "}
            </Text>
          </Button>
          <Button
            style={{
              flex: 1,
              padding: 10,
              height: 50,
              bottom: 0,
              marginHorizontal: 5,
              marginTop: 50,
              backgroundColor: '#FFB600',
              alignItems: "center",
              justifyContent: "center"
            }}
            onPress={this.props.handleSubmit(this.submit.bind(this))}
          >
            <Text style={{ color: "#000", fontWeight: "bold", fontSize: 18 }}>
              {" "}
              Next{" "}
            </Text>
          </Button>
        </View>
      </View>
    );
  }
}

CarForm = reduxForm({
  form: "carform", // a unique name for this form
  validate
})(CarForm);

CarForm = connect(state => ({
  user: state.driver.user,
  initialValues: {
    fname: state.driver.user.fname,
    lname: state.driver.user.lname,
    email: state.driver.user.email,
    phoneNo: state.driver.user.phoneNo,
    homeAddress: state.driver.user.homeAddress,
    emergencyDetails: state.driver.user.emergencyDetails
  }
}))(CarForm);

export default connect(mapStateToProps)(CarForm);

