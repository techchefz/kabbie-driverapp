import commonColor from '../../../../native-base-theme/variables/commonColor';
import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export default {
  iosHeader: {
    backgroundColor: '#FFB600'
  },
  aHeader: {
    backgroundColor: '#FFB600',
    borderColor: '#aaa',
    elevation: 3
  },
  iosHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    color: "#000"
  },
  aHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 26,
    marginTop: -5,
    color: "#000"
  },
  headerTitle: {
    fontWeight: 'bold',
    padding: 15,
    fontSize: 18,
    color: '#888'
  },
  boxcustom: {
    padding: 5,
    height: Dimensions.get('window').width * (1 / 2.5),
    width: Dimensions.get('window').width * (1 / 2.5),
    borderWidth: 1,
    borderRadius: 5,
    borderColor: 'gray',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20
  },
  listcustom: {
    borderBottomWidth: 2,
    borderBottomColor: '#ddd',
    borderTopWidth: 0,
    marginLeft: 0,
    paddingLeft: 15,
    backgroundColor: '#fff',
  },
  listContainer: {
    flexDirection: 'row'
  },
  lextText: {
    flex: 1,
    alignSelf: 'flex-start',
    alignItems: 'flex-start'
  },
  textColor: {
    color: '#444',
    fontSize: 16,
    alignSelf: 'flex-start',
    fontWeight: '600'
  },
  rightText: {
    alignSelf: 'flex-end',
    alignItems: 'flex-end'
  },
  buttonContinue: {
    alignSelf: 'stretch',
    borderRadius: 0,
    backgroundColor: '#FFB600'
  }
};