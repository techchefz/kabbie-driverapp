import React, { Component } from "react";
import { View, Platform, FlatList, TouchableOpacity, Dimensions, Image, SafeAreaView } from "react-native";
import { connect } from "react-redux";
import {
  Container,
  Header,
  Content,
  Button,
  Icon,
  ListItem,
  Text,
  Title,
  Left,
  Right,
  Body
} from "native-base";
import { Actions } from "react-native-router-flux";
import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";

class Documents extends Component {

  navigate(key, name) {
    Actions.uploadFiles({ keys: key, Filename: name });
  }

  renderArrow() {

    return (
      <Icon
        name="ios-arrow-forward"
        style={{
          ...styles.textColor,
          color: "#aaa",
          fontWeight: "bold",
          fontSize: 22
        }}
      />
    );
  }
  renderCheck() {
    return (
      <Icon
        name="checkmark"
        style={{
          ...styles.textColor,
          color: "#00C15E",
          fontWeight: "bold",
          fontSize: 22
        }}
      />
    );
  }

  renderIcon(key) {
    if (key === "licence") {
      return this.props.userDetails.licenceUrl
        ? this.renderCheck()
        : this.renderArrow();
    } else if (key === "insurance") {
      return this.props.userDetails.insuranceUrl
        ? this.renderCheck()
        : this.renderArrow();
    } else if (key === "permit") {
      return this.props.userDetails.vechilePaperUrl
        ? this.renderCheck()
        : this.renderArrow();
    } else if (key === "registration") {
      return this.props.userDetails.rcBookUrl
        ? this.renderCheck()
        : this.renderArrow();
    } else if (key === "photofront") {
      return this.props.userDetails.vechileFrontUrl
        ? this.renderCheck()
        : this.renderArrow();
    }
    else if (key === "photoback") {
      return this.props.userDetails.vechileBackUrl
        ? this.renderCheck()
        : this.renderArrow();
    }
  }
  renderRow = ({ item }) => {
    return (
      <ListItem
        style={styles.listcustom}
        onPress={() => this.navigate(item.key, item.name)}
      >
        <View style={styles.listContainer}>
          <View style={styles.lextText}>
            <Text style={styles.textColor}>{item.name}</Text>
          </View>

          <View style={styles.rightText}>{this.renderIcon(item.key)}</View>
        </View>
      </ListItem>
    );
  };
  onCotinueClick() {
    if(this.props.userDetails.licenceUrl == null){
      alert("Please upload Licence")
    } if(this.props.userDetails.insuranceUrl == null){
      alert("Please upload Insurance")
    } if(this.props.userDetails.vechilePaperUrl == null){
      alert("Please upload Vechile Paper")
    } if(this.props.userDetails.rcBookUrl == null){
      alert("Please upload RC Book")
    } if(this.props.userDetails.vechileFrontUrl == null){
      alert("Please upload Vechile Front")
    } if(this.props.userDetails.vechileBackUrl == null){
      alert("Please upload Vechile Back")
    } else {
      Actions.licenceDetails()
    }
  }
  render() {
    return (
      <Container style={{ backgroundColor: "#fff" }}>
        <SafeAreaView style={{ backgroundColor: "#FFB600", width: "100%" }} />
        <Header
          height={45}
          androidStatusBarColor="#FFB600"
          iosBarStyle="dark-content"
          style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}>
          <Left />
          <Body>
            <Title style={Platform.OS === "ios" ? styles.iosHeaderTitle : styles.aHeaderTitle}>
              Documents
            </Title>
          </Body>
          <Right />
        </Header>
        <Content style={{ backgroundColor: "#f2f4f6", padding: 15 }}>
          <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-around' }}>
            <TouchableOpacity
              onPress={() => this.navigate("licence", "Drivers License-Front")}
              style={styles.boxcustom}>
              <Image
                source={{ uri: this.props.userDetails.licenceUrl === null || undefined ? "https://cdn.shopify.com/s/files/1/0882/1686/products/solid_white_photo_studio_muslin_backdrop.jpg?v=1490271177" : this.props.userDetails.licenceUrl }}
                style={{
                  height: "100%",
                  width: "100%",
                  resizeMode: 'cover',
                }} />
              <Text style={{ color: this.props.userDetails.licenceUrl === null || undefined ? "#000" : "#fff", textAlign: 'center', position: 'absolute', top: '45%', backgroundColor: 'transparent' }}>Drivers License-Front</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.navigate("permit", "Contact Carriage Permit")}
              style={styles.boxcustom}>
              <Image
                source={{ uri: this.props.userDetails.vechilePaperUrl === null || undefined ? "https://cdn.shopify.com/s/files/1/0882/1686/products/solid_white_photo_studio_muslin_backdrop.jpg?v=1490271177" : this.props.userDetails.vechilePaperUrl }}
                style={{
                  height: "100%",
                  width: "100%",
                  resizeMode: 'cover',
                }} />
              <Text style={{ color: this.props.userDetails.vechilePaperUrl === null || undefined ? "#000" : "#fff", textAlign: 'center', position: 'absolute', top: '45%', backgroundColor: 'transparent' }}>Contact Carriage Permit</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.navigate("insurance", "Motor Insurance Certificate")}
              style={styles.boxcustom}>
              <Image
                source={{ uri: this.props.userDetails.insuranceUrl === null || undefined ? "https://cdn.shopify.com/s/files/1/0882/1686/products/solid_white_photo_studio_muslin_backdrop.jpg?v=1490271177" : this.props.userDetails.insuranceUrl }}
                style={{
                  height: "100%",
                  width: "100%",
                  resizeMode: 'cover',
                }} />
              <Text style={{ color: this.props.userDetails.insuranceUrl === null || undefined ? "#000" : "#fff", textAlign: 'center', position: 'absolute', top: '45%', backgroundColor: 'transparent' }}>Motor Insurance Certificate</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.navigate("registration", "Certificate of Registration")}
              style={styles.boxcustom}>
              <Image
                source={{ uri: this.props.userDetails.rcBookUrl === null || undefined ? "https://cdn.shopify.com/s/files/1/0882/1686/products/solid_white_photo_studio_muslin_backdrop.jpg?v=1490271177" : this.props.userDetails.rcBookUrl }}
                style={{
                  height: "100%",
                  width: "100%",
                  resizeMode: 'cover',
                }} />
              <Text style={{ color: this.props.userDetails.rcBookUrl === null || undefined ? "#000" : "#fff", textAlign: 'center', position: 'absolute', top: '45%', backgroundColor: 'transparent' }}>Certificate of Registration</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.navigate("photofront", "Vehicle Photo Front")}
              style={styles.boxcustom}>
              <Image
                source={{ uri: this.props.userDetails.vechileFrontUrl === null || undefined ? "https://cdn.shopify.com/s/files/1/0882/1686/products/solid_white_photo_studio_muslin_backdrop.jpg?v=1490271177" : this.props.userDetails.vechileFrontUrl }}
                style={{
                  height: "100%",
                  width: "100%",
                  resizeMode: 'cover',
                }} />
              <Text style={{ color: this.props.userDetails.vechileFrontUrl === null || undefined ? "#000" : "#fff", textAlign: 'center', position: 'absolute', top: '45%', backgroundColor: 'transparent' }}>Vehicle Photo Front</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.navigate("photoback", "Vehicle Photo Back")}
              style={styles.boxcustom}>
              <Image
                source={{ uri: this.props.userDetails.vechileBackUrl === null || undefined ? "https://cdn.shopify.com/s/files/1/0882/1686/products/solid_white_photo_studio_muslin_backdrop.jpg?v=1490271177" : this.props.userDetails.vechileBackUrl }}
                style={{
                  height: "100%",
                  width: "100%",
                  resizeMode: 'cover',
                }} />
              <Text style={{ color: this.props.userDetails.vechileBackUrl === null || undefined ? "#000" : "#fff", textAlign: 'center', position: 'absolute', top: '45%', backgroundColor: 'transparent' }}>Vehicle Photo Back</Text>
            </TouchableOpacity>
          </View>
        </Content>
        <SafeAreaView style={{ backgroundColor: "#FFB600", width: "100%" }} >
          <Button
            full
            // disabled={
            //   !(
            //     this.props.userDetails.licenceUrl &&
            //     this.props.userDetails.insuranceUrl &&
            //     this.props.userDetails.vechilePaperUrl &&
            //     this.props.userDetails.rcBookUrl &&
            //     this.props.userDetails.vechileFrontUrl &&
            //     this.props.userDetails.vechileBackUrl
            //   )
            // }
            style={styles.buttonContinue}
            onPress={() => 
              this.onCotinueClick()
            // Actions.licenceDetails()
            } >
            <Text style={{ alignSelf: "center", fontWeight: "bold", color:"#000" }}>
              Continue
          </Text>
          </Button>
        </SafeAreaView>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    userDetails: state.driver.user,
    licenceUrl: state.driver.user.licenceDetails.licenceUrl,
    insuranceUrl: state.driver.user.insuranceUrl,
    insuranceUrl: state.driver.user.vechilePaperUrl,
    insuranceUrl: state.driver.user.rcBookUrl,
    insuranceUrl: state.driver.user.vechileFrontUrl,
    insuranceUrl: state.driver.user.vechileBackUrl,
    profileUpdating: state.driver.user.profileUpdating
  };
}

export default connect(mapStateToProps, null)(Documents);
