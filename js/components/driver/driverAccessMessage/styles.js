import commonColor from "../../../../native-base-theme/variables/commonColor";
import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
export default {
  iosHeader: {
    backgroundColor: "#FFB600"
  },
  aHeader: {
    backgroundColor: "#FFB600",
    borderColor: "#aaa",
    elevation: 3
  },
  iosHeaderTitle: {
    fontSize: 18,
    fontWeight: "500",
    color: "#000"
  },
  verifyText: {
    fontSize: 20,
    fontWeight: "500",
    color: "#666666",
    marginTop: 30,
    marginHorizontal: 20
  },
  aHeaderTitle: {
    fontSize: 18,
    fontWeight: "500",
    lineHeight: 26,
    marginTop: -5,
    color: "#000"
  },
  buttonContinue: {
    backgroundColor: "#FFB600",
    alignSelf: "stretch",
    borderRadius: 0
  }
};
