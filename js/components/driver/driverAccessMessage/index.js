import React, { Component } from "react";
import { Platform, Dimensions, SafeAreaView } from "react-native";
import {
  Container,
  Header,
  Content,
  Button,
  Icon,
  Card,
  CardItem,
  Text,
  Left,
  Right,
  Title,
  Body
} from "native-base";
import { Actions, ActionConst } from "react-native-router-flux";
import styles from "./styles";
import commonColor from "../../../../native-base-theme/variables/commonColor";
import MIcon from "react-native-vector-icons/MaterialIcons";
import View from "../../../../native-base-theme/components/View";
const { width, height } = Dimensions.get("window");

class DriverAccessMessage extends Component {
  render() {
    
    return (
      <Container style={{ flex:1, height: Dimensions.get("screen").height, backgroundColor: "#fff" }}>
      <SafeAreaView style={{backgroundColor:"#FFB600"}}/>
        <Header
          iosBarStyle="dark-content"
          androidStatusBarColor="#FFB600"
          style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
        >
          <Body>
            <Title
              style={
                Platform.OS === "ios" ? (
                  styles.iosHeaderTitle
                ) : (
                  styles.aHeaderTitle
                )
              }
            >
              Registration Approval
            </Title>
          </Body>
        </Header>
        <Content style={{ flex:1, height:"100%", backgroundColor: "#f2f4f6" }}>
          <Text style={styles.verifyText}>
            Your account will be updated in next 24Hrs.
          </Text>
          <MIcon name="schedule" size={260} style={{alignSelf:"center"}} color="red"/>
          <Text 
          
          style={{fontSize:22, color:"#666666", alignSelf:"center", marginTop:20}}></Text>
          <Button
          block
          style={[styles.buttonContinue, {
            width:"60%", 
            alignSelf:"center",
            borderRadius:10,
            ...Platform.select({
              android: { elevation: 2 },
              ios: {
                shadowColor: 'black',
                shadowOffset: { height: 1, width: 1 },
                shadowOpacity: 1,
                shadowRadius: 1,
              }
            }),
          }]}
          onPress={()=> Actions.documents()}
        >
          <Text style={{ alignSelf: "center", fontWeight: "bold", color:"#000" }}>Re-Submit your Details</Text>
        </Button>
        </Content>
        <Button
          block
          style={styles.buttonContinue}
          onPress={() => Actions.login({ type: ActionConst.RESET })}
        >
          <Text style={{ alignSelf: "center", fontWeight: "bold", color:"#000" }}>Back</Text>
        </Button>
        {/* </Content> */}
        <SafeAreaView style={{backgroundColor:"#FFB600"}}/>
      </Container>
    );
  }
}

export default DriverAccessMessage;
