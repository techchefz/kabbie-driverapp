//import liraries
import React, { Component } from 'react';
import { View, Text, Platform, Dimensions, TouchableOpacity, Image, StyleSheet, ScrollView, SafeAreaView} from 'react-native'
import {
    Container,
    Header,
    Content,
    Button,
    Title,
    Left,
    Right,
    Body,
    Icon,
    Grid, Row, Col
} from "native-base";
import config from "../../../../config";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import styles from "./styles";

function mapStateToProps(state) {
    return {
        jwtAccessToken: state.driver.appState.jwtAccessToken,
        email:state.driver.user.email,
        _id: state.driver.user._id,
    };
}


// create a component
class MyComplaints extends Component {
    constructor(props) {
        super(props)

        this.state = {
            queries: []
        }
    }
    componentWillMount() {
        fetch(`${config.serverSideUrl}:${config.port}/api/query/getQuery`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: this.props.jwtAccessToken,
                _id: this.props._id
            }
        })
            .then(resp => resp.json())
            .then(data => {
                this.setState({ queries: data.data })
                console.log('================data====================');
                console.log(data);
            })
            .catch(e => e);
    }

    render() {
        return (
            <View style={styless.container}>
            <SafeAreaView style={{backgroundColor:"#FFB600", width:"100%"}}>
                <Header
                    iosBarStyle="dark-content"
                    androidStatusBarColor="#FFB600"
                    style={{
                        width: "100%",
                        backgroundColor: "#FFB600",

                    }}
                >
                    <Left>
                        <Button transparent onPress={() => Actions.pop()}>
                            <Icon
                                name="md-arrow-back"
                                style={{ fontSize: 28, color: "#000" }}
                            />
                        </Button>
                    </Left>
                    <Body>
                        <Title
                            style={
                                Platform.OS === "ios"
                                    ? styles.iosHeaderTitle
                                    : styles.aHeaderTitle
                            }
                        >
                            My Queries
            </Title>
                    </Body>
                    <Right />
                </Header>
                </SafeAreaView>
                {this.state.queries.length == 0 ?
                 <View style={{flex:1, backgroundColor:"#fff", width:"100%", justifyContent:"space-between"}}>
                 <Text style={{textAlign:"center", marginTop:50, fontSize:30, fontWeight:"100"}}>Where's your Query....? I think its empty</Text>
                    <Image
                    source={require("../../../../assets/images/noQueries.jpg")}
                    style={{
                        width:"100%",
                        height:350,
                        resizeMode:"contain",
                        alignSelf:"center",
                    }}/>
                 </View>
                 : 
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, width: "100%", backgroundColor: "#e4e4e4" }}>
                    {this.state.queries.map((data, i) =>
                        <View style={{
                            width: "90%",
                            padding: 10,
                            marginBottom: 5,
                            backgroundColor: "white",
                            borderRadius: 2,
                            marginTop: 5,
                            justifyContent: "center",
                            // alignItems: "center",
                            alignSelf: "center",
                            ...Platform.select({
                                ios: {
                                    shadowColor: '#000',
                                    shadowOffset: {
                                        width: 2,
                                        height: 2
                                    },
                                    shadowRadius: 1,
                                    shadowOpacity: 0.5
                                },
                                android: { elevation: 4 }
                            })
                        }}>
                            <Grid>
                                <Row>
                                    <Col style={{ flex: 1 }}>
                                        <Text style={{ fontWeight: "bold" }}>Subject </Text>
                                    </Col>
                                    <Col style={{ flex: 0.1 }}><Text>:</Text></Col>
                                    <Col style={{ flex: 3.5 }}>
                                        <Text>{data.querySubject}</Text>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col style={{ flex: 1 }}>
                                        <Text style={{ fontWeight: "bold" }}>Query </Text>
                                    </Col>
                                    <Col style={{ flex: 0.1 }}><Text>:</Text></Col>
                                    <Col style={{ flex: 3.5 }}>
                                        <Text>{data.queryMessage.trim()}</Text>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col style={{ flex: 1 }}>
                                        <Text style={{ fontWeight: "bold" }}>Status </Text>
                                    </Col>
                                    <Col style={{ flex: 0.1 }}><Text>:</Text></Col>
                                    <Col style={{ flex: 3.5 }}>
                                        <Text><FIcon name="dot-circle-o" color={data.queryStatus == "In Progress" ? "orange" : 
                                        data.queryStatus == "Resolved" ? "green" : "red" 
                                    }/>  {data.queryStatus}</Text>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col style={{ flex: 1 }}>
                                        <Text style={{ fontWeight: "bold" }}>Date </Text>
                                    </Col>
                                    <Col style={{ flex: 0.1 }}><Text>:</Text></Col>
                                    <Col style={{ flex: 3.5 }}>
                                        <Text>{data.submittedOn.slice(0, 10)}</Text>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col style={{ flex: 1 }}>
                                        <Text style={{ fontWeight: "bold" }}>Time </Text>
                                    </Col>
                                    <Col style={{ flex: 0.1 }}><Text>:</Text></Col>
                                    <Col style={{ flex: 3.5 }}>
                                        <Text>{data.submittedOn.slice(11, 19)}</Text>
                                    </Col>
                                </Row>

                            </Grid>
                        </View>
                    )}
                </ScrollView>
                }
            </View>
        );
    }
}

// define your styles
const styless = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default connect(mapStateToProps)(MyComplaints);
