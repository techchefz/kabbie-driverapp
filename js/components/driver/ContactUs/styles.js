import commonColor from '../../../../native-base-theme/variables/commonColor';

const React = require('react-native');

const {Dimensions} = React;

const deviceHeight = Dimensions.get('window').height;

export default {
  iosHeader: {
    backgroundColor: '#FFB600',
  },
  aHeader: {
    backgroundColor: '#FFB600',
    borderColor: '#aaa',
    elevation: 3,
  },
  iosHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    color: "#000",
  },
  aHeaderTitle: {
    fontSize: 18,
    fontWeight: '500',
    lineHeight: 26,
    marginTop: -5,
    color: "#000",
  },
  
};
