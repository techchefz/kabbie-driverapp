import React, { Component } from 'react'
import { View, Platform, Dimensions, TouchableOpacity, Image, StyleSheet, SafeAreaView, ScrollView, KeyboardAvoidingView } from 'react-native'
import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Thumbnail,
  Card,
  CardItem,
  Title,
  Left,
  Right,
  Body,
  List,
  ListItem,
  Form,
  Item,
  Input,
  Label,
  Toast
} from "native-base";
import styles from "./styles";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import config from "../../../../config";

function mapStateToProps(state) {
  return {
    jwtAccessToken: state.driver.appState.jwtAccessToken,
    _id: state.driver.user._id,
    email: state.driver.user.email,
  };
}

export class ContactUs extends Component {
  constructor(props) {
    super(props)

    this.state = {
      _id: this.props._id,
      subject: null,
      message: null,
      showToast: false
    }
  }

  sendMessage() {
    fetch(`${config.serverSideUrl}:${config.port}/api/query/newQuery`,
      {
        method: 'POST',
        headers: {
          "Content-Type": "application/json",
          Authorization: this.props.jwtAccessToken,
        },
        body: JSON.stringify({ _id: this.state._id, subject: this.state.subject, message: this.state.message })
      })
      .then(resp => resp.json()).then((convertedData) => {
        console.log('=================convertedData===================')
        console.log(convertedData.success);
        console.log('=================convertedData===================')
        if (convertedData.success === true) {
          Toast.show({
            text: " you'll be contacted soon",
            duration: 10000,
            buttonText: 'Okay'
          }),
            Actions.pop()
        }
      })
  }

  onSend() {
    if (this.state.subject === null || this.state.message === null) {
      alert("All fields are mandatory")
    }
    else {
      this.sendMessage()
    }
  }
  render() {

    return (
      <View>
        <SafeAreaView style={{ backgroundColor: "#FFB600", width: "100%" }} />
        <Header
          height={45}
          iosBarStyle="dark-content"
          androidStatusBarColor="#FFB600"
          style={Platform.OS === "ios" ? styles.iosHeader : styles.aHeader}
        >
          <Left>
            <Button transparent onPress={() => Actions.pop()}>
              <Icon
                name="md-arrow-back"
                style={{ fontSize: 28, color: "#000" }}
              />
            </Button>
          </Left>
          <Body>
            <Title
              style={
                Platform.OS === "ios"
                  ? styles.iosHeaderTitle
                  : styles.aHeaderTitle
              }
            >
              Contact Us
            </Title>
          </Body>
          <Right />
        </Header>
        <View style={{ height: Dimensions.get("screen").height, backgroundColor: 'white' }}>
        <KeyboardAvoidingView behavior="padding">
          <ScrollView scrollEnabled={true}>
            <Form>
              <Item floatingLabel style={{ width: "80%" }}>
                <Label>Email</Label>
                <Input
                  style={{ paddingLeft: 10 }}
                  editable={false}
                  value={this.props.email}
                  onChangeText={(email) => this.setState({ email })} />
              </Item>
              <Item floatingLabel style={{ width: "80%" }}>
                <Label>Subject</Label>
                <Input
                  style={{ paddingLeft: 10 }}
                  multiline={true}
                  onChangeText={(subject) => this.setState({ subject })} />
              </Item>
            </Form>
            <View style={{
              padding: 10,
              height: 300,
              marginTop: 20
            }}>
              <Item regular style={{ width: "100%", height: "100%" }}>
                <Input
                  multiline={true}
                  style={{
                    height: "100%"
                  }}
                  placeholder='Write your message'
                  onChangeText={(message) => this.setState({ message })} />
              </Item>
            </View>
            <Button
              onPress={() => this.onSend()}
              style={{ width: 200, alignSelf: "center", backgroundColor: "#FFB600", marginTop: 20 }}>
              <Text style={{ textAlign: "center", width: "100%", color: "#000" }}>SEND</Text>
            </Button>
            <Button
              onPress={() => Actions.myQuery()}
              style={{ width: 200, marginBottom: 90, alignSelf: "center", backgroundColor: "transparent", marginTop: 20 }}>
              <Text style={styless.queryStatus}>Show Query status</Text>
            </Button>
          </ScrollView>
          </KeyboardAvoidingView>
        </View>
      </View>
    )
  }
}

const styless = StyleSheet.create({
  queryStatus: {
    textAlign: "center",
    width: "100%",
    color: "#000"
  }
})

export default connect(mapStateToProps)(ContactUs);
